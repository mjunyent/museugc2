#include <QtGui/QMouseEvent>
#include "GLWidget.h"
#include <QApplication>
#include <QTimer>
#include <iostream>
GLWidget::GLWidget(QWidget *parent) : QGLWidget(parent)
{
    setMouseTracking(true);
    controlTeclatIRatoli = true;
    mostrantMenu = false;
    //És necessari crear aquí el client, ja que ha d'apuntar a alguna cosa quan es faci el connect a museuwindow.cpp
    //per poder utilitzar signals/slots.
    pers = new Personatge();
    sight3rd = 0.0f;
    client = new Client(this, pers);
    quadreInfo = NULL; //si imatge apunta a alguna cosa, la mostrarem, sino mostrarem el museu normal
}

GLWidget::~GLWidget()
{
    if (camera)
        delete camera;
    if (pers)
        delete pers;
    if (shader)
        delete shader;
    if (client)
        delete client;
    if (timerGeneral)
        delete timerGeneral;
    //s'han d'incloure tots els frees i deletes!!!!
    // ...
    // ...
}

/*****************************************************************************
 * initializeGL()
 *      Called at context creation. Sets initial state.
 *****************************************************************************/
void GLWidget::initializeGL()
{

#ifndef __APPLE__

    // 0) OpenGL 2.0 support test
    //    (needed for shading)
    //----------------------------------------------------------

    glewInit();

    if (glewIsSupported("GL_VERSION_2_0"))
    {
        qDebug() << "Ready for OpenGL 2.0";
        openGL2OK = true;
    }
    else
    {
        qDebug() << "OpenGL 2.0 not supported";
        openGL2OK = false;
    }

#endif

    // 1) Go for a well known initialization state.
    //    (essential stuff only)
    //----------------------------------------------------------

    // 1a) Setting default values to clean buffers
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClearDepth(1.0f); // This is the default OpenGL value, but I'm making it explicit for you

    // 1b) Backface culling enabled
    //     (this will speedup rendering)
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);

    // 1c) Enable depth testing
    glEnable(GL_DEPTH_TEST);
    //glDepthFunc(GL_LEQUAL);

    //Enable lighting
    glEnable(GL_LIGHTING);

    //Activem textures
    glEnable(GL_TEXTURE_2D);

    // 2) Init our own architecture (camera, lights, action!)
    //----------------------------------------------------------
    camera = new Camera();
    frustum = new Frustum();
	camera->setProjection(this->w,this->h);

    // 3) Init shaders
    //----------------------------------------------------------
    shader = new Shader(openGL2OK);
    shader->inicialitzaShaders();
    shadersON = true;

    // Generem el mapa
    mapa = new Mapa();
    glEnable(GL_SCISSOR_TEST);

    //Inicialització timer
    timerActualitzacio = new QTimer(this);
    connect(timerActualitzacio, SIGNAL(timeout()), this, SLOT(processaTimer()));
    timerActualitzacio->start(1000/FPS_MAX); //ex. 60 frames / 1 segon =>  1 segon / 60 frames => 1/60 segons/ 1 frame => 1000/60 milisegons cada frame
    timerGeneral = new QElapsedTimer();
    timerGeneral->start();

    modPers=false;
    importador = new Assimp::Importer();

    if(!loadMuseu("museu.mu")){
        qDebug() << "El museu no s'ha carregat correctament.";
        exit(-1);
    }

    pers->setPersona(&Persones[0]);
    this->mapa->generaMapaColisions(Entitats, Quadres, shader);
}

/*****************************************************************************
 * resizeGL()
 *      Called at context resizing. Controls aspect ratio changes.
 *****************************************************************************/
void GLWidget::resizeGL(int w, int h)
{
    this->w= w;
    this->h= h;
    glViewport(0,0,w,h);
    camera->setProjection(w,h);
}

/*****************************************************************************
 * paintGL()
 *      Called automatically after init, on possible clock events,
 *      or after UpdateGL requests. Controls context painting.
 *****************************************************************************/
void GLWidget::paintGL()
{
    double timeElapsed = timerGeneral->restart() / 1000.0; //ho passem a segons

    glViewport(0,0,this->w,this->h);
    glScissor(0,0,this->w,this->h);

    if(quadreInfo==NULL){
        //No hi ha cap imatge a mostrar, mostrem el museu tal qual
        //activem els shaders
        if(shadersON)
            shader->actiu = true; //activem els shaders
        else{
            shader->actiu = false;
            glUseProgram(0); //desactivem el shader (nomes ho podem fer des d'aqui, sino aixo seria a la classe shader :/)
        }

        // Clean buffers:
        // COLOR to ensure final representation has no waste from previous renders
        // DEPTH to ensure correct representation of objects given the depht testing used
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

        camera->setProjection(this->w,this->h); // Més endevant es podrà posar al initialize, després de generar el mapa.

        // Update character with its current position
        pers->update(timeElapsed);
        // Update camera to its current position
        camera->update(pers,sight3rd);

        //Agafem el frustum
        frustum->extractFrustum();
        for(unsigned int i=0; i<Entitats.size(); i++){
            Entitats[i].dibuixa(shader, frustum);
        }
        for(unsigned int i=0; i<Quadres.size(); i++){
            Quadres[i].dibuixa(shader, frustum);
        }
        if(client->connectat && client->loggedIn){
            //Processem tots els missatges que ens han arribat fins ara i dibuixem els usuaris
            client->processaMissatges();
            client->dibuixaUsuaris();
        }
        pers->dibuixa();

        // PRINTEM MAPA
        mapa->Dibuixa(camera, pers, this->w,this->h, Entitats, Quadres, shader, client->users);
    }else{
        //Hi ha una imatge a mostrar!
        shader->actiu = false;
        glUseProgram(0); //desactivem el shader (nomes ho podem fer des d'aqui, sino aixo seria a la classe shader :/)
        glDisable(GL_TEXTURE_2D);
        quadreInfo->showInfo(w,h);
        glEnable(GL_TEXTURE_2D);
    }
}

void GLWidget::processaTimer(){
    updateGL();
    if(modPers || pers->roth!=0){
        client->enviaPosicio();
        modPers=false;
    }
}

/*****************************************************************************
 * mouseMoveEvent()
 *      Called on mouse move. Controls this input.
 *      Mapejem posicio (posx,posy) ratoli dins la pantalla
 *                ____________________
 *               |(0,0)               |
 *               |                    |
 *               |                    |
 *               |                    |
 *               |                    |
 *               |                    |
 *               |_______________(1,1)|
 *
 *****************************************************************************/
void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(controlTeclatIRatoli){
        //bool update = false;
        float posx=0.0f, posy=0.0f;

        pers->roth=0.0f;
        pers->rotv=0.0f;

        posx=event->pos().x()/this->w;
        posy=event->pos().y()/this->h;
        //qDebug() << "Pos camera" << posx << posy;
        if (posx<0.25) pers->roth=-2*(1-posx);
        if (posx>0.75) pers->roth=2*posx;
        //Limitem la orientaciov perque simuli el moviment possible d'un huma
        if (posy<0.25) pers->rotv=-1.5*(1-posy);
        if (posy>0.75) pers->rotv=1.5*posy;
        modPers=true;
    }
}
void GLWidget::wheelEvent(QWheelEvent *event)
{

    float aux=sight3rd;
    //Falta comprovar que el qTimer
    if(event->orientation() == Qt::Vertical)
    {
        if(event->delta()>0){
            this->sight3rd -= ZOOM_WHEEL;
        } else {
            this->sight3rd += ZOOM_WHEEL;
        }
    }
    if(this->sight3rd<MAXIM_A_PROP){
        if(aux < this->sight3rd) this->sight3rd = MAXIM_A_PROP; // passem de primera a tercera persona
        else this->sight3rd = 0.0; //passem de tercera persona a primera
    }
    if(this->sight3rd>MAXIM_LLUNY)this->sight3rd = MAXIM_LLUNY; //no deixem que la camera s'allunyi més
}

/*****************************************************************************
 * keyPressEvent()
 *      Es crida quan premem una tecla. Mapejem les tecles del cursor per
 *moure el personatge, tambe W A S D
 *             ________
 *            |        |
 *            |endavant|
 *   ________ |________| _________
 *  |        ||        ||         |
 *  |esquerra||enrere  ||dreta    |
 *  |________||________||_________|
 *
 *
 *  ENTER: escriure al xat
 *
 *  ESC: mostrar el menu
 *
 *  ESPAI: si estem mirant un quadre, mostrar la seva informació
 *
 *  P o p: Canviar el mode de vista entre primera persona i tercera persona.
 *
 *****************************************************************************/

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    GLfloat xantic=pers->x;
    GLfloat zantic=pers->z;
    GLfloat *xnou,*znou;
    float distancia, x, y, z, producteEscalar, cosAngle, maxCosAngle;
    if(controlTeclatIRatoli){
        switch(event->key()) {
        case Qt::Key_Up:
        case Qt::Key_W:
            pers->mouEndavant();
            break;
        case Qt::Key_Down:
        case Qt::Key_S:
            pers->mouEnrere();
            break;
        case Qt::Key_Right:
        case Qt::Key_D:
            pers->mouDreta();
            break;
        case Qt::Key_Left:
        case Qt::Key_A:
            pers->mouEsquerra();
            break;
        case Qt::Key_Escape:
            //en cas de premer ESC mostrem el menu
            emit mostraMenu();
            mostrantMenu = true;
            controlTeclatIRatoli = false;
            break;
        case Qt::Key_Return:
        case Qt::Key_Enter:
            emit escriureXat();
            break;
        case Qt::Key_Space:
            maxCosAngle = 0.0;
            //Mirem per tots els quadres, si son al frustum (al camp de visió)
            for(unsigned int i=0; i<Quadres.size(); i++){
                if(Quadres[i].hasInfo){
                    //El quadre té una textura d'informació
                    if(frustum->pointInFrustum(Quadres[i].centre[0], Quadres[i].centre[1], Quadres[i].centre[2])){
                        //el quadre és al nostre camp de visió
                        //creem un vector que va del punt central del quadre cap a nosaltres
                        x = pers->x - Quadres[i].centre[0];
                        y = pers->y - Quadres[i].centre[1];
                        z = pers->z - Quadres[i].centre[2];
                        //calculem el modul d'aquest vector
                        distancia = sqrt(x*x+y*y+z*z);
                        if(distancia <= DISTANCIA_INFO_QUADRE){
                            //som a una distancia prou petita
                            //mirem si el quadre esta orientat cap a nosaltres o no (podria ser que estigues darrere d'una paret)
                            //ho fem calculant el cosinus de l'angle entre el vector normal del quadre i el vector que hem creat
                            producteEscalar = x*Quadres[i].normal[0] + y*Quadres[i].normal[1] + z*Quadres[i].normal[2];
                            //el cosinus de l'angle és el producte escalar entre la multiplicació de moduls (modul de la normal = 1)
                            //aux conté el cosinus de l'angle amb el quadre anterior, el que tingui el cosinus més proper a 1 guanya!
                            //a més això ens assegura que el quadre mira cap a nosaltres ja que maxCosAngle comença sent 0
                            cosAngle = producteEscalar/distancia;
                            if(cosAngle > maxCosAngle){
                                quadreInfo = &(Quadres[i]);
                                controlTeclatIRatoli = false; //fem que no ens poguem moure
                                maxCosAngle = cosAngle;
                                if(client->connectat && client->loggedIn)
                                    emit amagaXat();
                            }
                        }
                    }
                }
            }
            break;
        default:
            break;
        }
        modPers=true;
        if(mapa->colisions){

            pers->calculaPassaFutura(xnou,znou);
            if (mapa->calculaColisions(*xnou,*znou)==false){
                pers->x=xantic;
                pers->z=zantic;
            }
        }
    } else {
        //cas que no ens poguem moure
        if(event->key() == Qt::Key_Escape){
            //En aquest cas no tenim el control del teclat i ens arriba un ESC
            if(mostrantMenu){
                //Vol dir que s'esta mostrant el menu
                //Per tant emetem una senyal que rebra museuwindow per a continuar (treure el menu)
                emit continua();
            }else{
                //cas que no estiguem mostrant el menu, pero no tenim el control del teclat i el ratoli (per exemple hi ha una imatge d'info d'un quadre)
                //mostrem el menu
                emit mostraMenu();
                mostrantMenu = true;
            }
        }else if(event->key() == Qt::Key_Space){
            if(quadreInfo != NULL){ //si no ens podem moure i hi ha una imatge, la treiem
                quadreInfo = NULL;
                controlTeclatIRatoli = true; //agafem el control només si no estem mostrant cap imatge
                if(client->connectat && client->loggedIn)
                    emit mostraXat();
            }
        }
    }

}

void GLWidget::activarControl(){
    //venim del menu
    if(quadreInfo == NULL)
        controlTeclatIRatoli = true; //agafem el control només si no estem mostrant cap imatge
    mostrantMenu = false;
}

bool GLWidget::loadMuseu(std::string fileName){
    FILE* file;
    char line[500];
    char nom[32];
    int size = sizeof(line);
    char *aux;
    char *token;
    char *token2;
    Objecte *obj = NULL;
    Entitat *ent = NULL;
    bool construintEntitat = false;
    int llumID = 0;
    bool personatgeDonat = false;
    bool colisions = false;
    bool brilla = false;

    /*
     * Tal com està implementat, utilitzant " \t\n" com a delimitadors,
     * strsep(&aux, " \t\n") retornarà un punter a un string que
     * comença per /0 quan s'arriba al final de la línia.
     * Si després es torna a utilitzar, retornarà un punter a NULL.
     * NULL és 0 en binari, si accedim a aquest punter tindrem un segmentation fault!
     *
     */

    if((file = fopen(fileName.c_str(), "r")) != NULL){
        while(fgets(line, size, file) != NULL){
            //si no és un comentari, seguim processant la línia
            aux = line; //fem que aux apunti a line
            while(*aux == '\t' || *aux == ' ') aux++; //treiem les tabulacions o els espais del principi de la linia
            if(*aux != '#'){ //si es tracta d'un comentari, ignorem la línia
                //agafem el primer token
                token = strsep(&aux, " \t\n");
                if(!(token == NULL || *token == '\0')){ //si es tracta d'una línia en blanc, la ignorem
                    if(strcmp(token, "enable") == 0 || strcmp(token, "Enable") == 0 || strcmp(token, "ENABLE") == 0){
                        getPropietat(true, aux, &colisions, &brilla);
                    }else if(strcmp(token, "disable") == 0 || strcmp(token, "Disable") == 0 || strcmp(token, "DISABLE") == 0){
                        getPropietat(false, aux, &colisions, &brilla);
                    }else if(strcmp(token, "entity") == 0 || strcmp(token, "Entity") == 0 || strcmp(token, "ENTITY") == 0){
                        //ENTITAT
                        //Comprovem que no hi hagi cap altra entitat en construcció
                        if(!construintEntitat){
                            construintEntitat = true;
                            //El següent token serà o bé el tipus de la entitat o bé el nom.
                            if(*(token = strsep(&aux, " \t\n")) == '\0') return false; //el primer token sempre ha d'existir
                            strcpy(nom, token);
                            token2 = strsep(&aux, " \t\n");
                            if (*token2 == '\0'){
                                //ENTITAT NORMAL
                                //token conté el nom, es tracta d'una entitat normal: Entity parets
                                ent = new Entitat();
                                memcpy(ent->nom, nom, sizeof(ent->nom));
                                ent->nom[sizeof(ent->nom)-1]='\0'; //per si de cas acabem l'string
                                //no hi haurà res més perquè token2 == '\0'
                            } else {
                                strcpy(nom, token2);
                                //token conté el tipus i token2 el nom, es tracta d'una entitat especial: Entity Personatge persona
                                if(strcmp(token, "light") == 0 || strcmp(token, "Light") == 0 || strcmp(token, "LIGHT") == 0){
                                    //ENTITAT LIGHT
                                    float dif[4] = {-1., -1., -1., -1.};
                                    float amb[4] = {-1., -1., -1., -1.};
                                    float spe[4] = {-1., -1., -1., -1.};
                                    Objecte* spotlight = NULL;
                                    //Es tracta de la definició d'un llum
                                    if(llumID+1 < MAX_LLUMS){
                                        //A la següent linia tindrem els valors pos, dif, amb i spe
                                        for(int i=0; i<4; i++){
                                            if(fgets(line, size, file) != NULL){
                                                aux = line; //fem que aux apunti a line
                                                while(*aux == '\t' || *aux == ' ') aux++; //treiem les tabulacions o els espais del principi de la linia
                                                if(*aux != '#'){ //comentari?
                                                    //agafem el primer token
                                                    token = strsep(&aux, " \t\n");
                                                    if(!(token == NULL || *token == '\0')){ //linia en blanc?
                                                        if(strcmp(token, "enable") == 0 || strcmp(token, "Enable") == 0 || strcmp(token, "ENABLE") == 0){
                                                            getPropietat(true, aux, &colisions, &brilla);
                                                            i--; //aixo no estava previst
                                                        }else if(strcmp(token, "disable") == 0 || strcmp(token, "Disable") == 0 || strcmp(token, "DISABLE") == 0){
                                                            getPropietat(false, aux, &colisions, &brilla);
                                                            i--;
                                                        }else if(strcmp(token, "difuse") == 0 || strcmp(token, "Difuse") == 0 || strcmp(token, "DIFUSE") == 0){
                                                            if(!llegirVector(aux, dif, 4))
                                                                return false;
                                                        }else if(strcmp(token, "ambient") == 0 || strcmp(token, "Ambient") == 0 || strcmp(token, "AMBIENT") == 0){
                                                            if(!llegirVector(aux, amb, 4))
                                                                return false;
                                                        }else if(strcmp(token, "specular") == 0 || strcmp(token, "Specular") == 0 || strcmp(token, "SPECULAR") == 0){
                                                            if(!llegirVector(aux, spe, 4))
                                                                return false;
                                                        }else if(strcmp(token, "spotlight") == 0 || strcmp(token, "Spotlight") == 0 || strcmp(token, "SpotLight") == 0 || strcmp(token, "SPOTLIGHT") == 0){
                                                            spotlight = new Objecte(importador);
                                                            strcpy(spotlight->nom, "spotlight");
                                                            if(!llegirObjecte(spotlight, aux, colisions, brilla))
                                                                return false;
                                                        }else{
                                                            return false;
                                                        }
                                                    }else{
                                                        i--; //si es tracta d'una línia en blanc, la ignorem
                                                    }
                                                }else{
                                                    i--; //si es tracta d'un comentari, ignorem la línia
                                                }
                                            }else{
                                                return false; //si fgets retorna null vol dir que s'ha acabat el fitxer
                                            }
                                        }
                                        if(spe[0] != -1. && amb[0] != -1 && dif[0] != -1 && spotlight != NULL){
                                            //creem l'entitat de tipus Llum
                                            ent = new Llum(spotlight->centre, dif, amb, spe, llumID);
                                            ent->objecte.push_back(*spotlight);
                                            memcpy(ent->nom, nom, sizeof(ent->nom)); //token2 contenia el nom
                                            ent->nom[sizeof(ent->nom)-1]='\0'; //per si de cas acabem l'string
                                            llumID++;
                                        }else{
                                            qDebug()<<"No hem omplert bé difuse, specular, spotlight, etc";
                                            return false; //si no hem omplert algun dels 4 vectors retornem false
                                        }
                                    }else{
                                        qDebug() << "massa llums, maxim: " << MAX_LLUMS;
                                        return false; //si ens passem del numero de llums maxim retornem false
                                    }
                                }
                            }
                        } else {
                            qDebug() << "falta l'END!";
                            //hem trobat un ENTITY abans d'un END
                            return false;
                        }
                    } else if(strcmp(token, "object") == 0 || strcmp(token, "Object") == 0 || strcmp(token, "OBJECT") == 0){
                        //OBJECTE
                        obj = new Objecte(importador);
                        if(*(token = strsep(&aux, " \t\n")) == '\0') return false; //llegim el nom
                        memcpy(obj->nom, token, sizeof(obj->nom));
                        obj->nom[sizeof(obj->nom)-1]='\0'; //per si de cas acabem l'string
                        if(!llegirObjecte(obj, aux, colisions, brilla))
                            return false;
                        ent->objecte.push_back(*obj);
                    } else if(strcmp(token, "end") == 0 || strcmp(token, "End") == 0 || strcmp(token, "END") == 0){
                        //END
                        Entitats.push_back(*ent);
                        construintEntitat = false;
                        if(*strsep(&aux, " \t\n") != '\0') return false; //Comprovem que no hi hagi res més a la línia
                    } else if(strcmp(token, "picture") == 0 || strcmp(token, "Picture") == 0 || strcmp(token, "PICTURE") == 0){
                        //QUADRE
                        Objecte* pintura = NULL;
                        Objecte* marc = NULL;
                        Quadre* quadre = NULL;
                        QImage* info = NULL;

                        if(*(token2 = strsep(&aux, " \t\n")) == '\0') return false; //el nom sempre ha d'existir
                        strcpy(nom, token2);
                        if(*strsep(&aux, " \t\n") != '\0') return false; //Comprovem que no hi hagi res més a la línia
                        //A la següent linia tindrem 2 objectes painting i frame, una imatge i despres un end
                        for(int i=0; i<3; i++){
                            if(fgets(line, size, file) != NULL){
                                aux = line; //fem que aux apunti a line
                                while(*aux == '\t' || *aux == ' ') aux++; //treiem les tabulacions o els espais del principi de la linia
                                if(*aux != '#'){ //comentari?
                                    //agafem el primer token
                                    token = strsep(&aux, " \t\n");
                                    if(!(token == NULL || *token == '\0')){ //linia en blanc?
                                        if(strcmp(token, "enable") == 0 || strcmp(token, "Enable") == 0 || strcmp(token, "ENABLE") == 0){
                                            getPropietat(true, aux, &colisions, &brilla);
                                            i--; //aixo no estava previst
                                        }else if(strcmp(token, "disable") == 0 || strcmp(token, "Disable") == 0 || strcmp(token, "DISABLE") == 0){
                                            getPropietat(false, aux, &colisions, &brilla);
                                            i--;
                                        }else if(strcmp(token, "painting") == 0 || strcmp(token, "Painting") == 0 || strcmp(token, "PAINTING") == 0){
                                            pintura = new Objecte(importador);
                                            strcpy(pintura->nom, "painting");
                                            if(!llegirObjecte(pintura, aux, colisions,brilla))
                                                return false;
                                        }else if(strcmp(token, "frame") == 0 || strcmp(token, "Frame") == 0 || strcmp(token, "FRAME") == 0){
                                            marc = new Objecte(importador);
                                            strcpy(marc->nom, "frame");
                                            if(!llegirObjecte(marc, aux, colisions,brilla))
                                                return false;
                                        }else if(strcmp(token, "information") == 0 || strcmp(token, "Information") == 0 || strcmp(token, "INFORMATION") == 0){
                                            token = strsep(&aux, " \t\n"); //llegim el path de la textura
                                            //Llegim la textura
                                            info = new QImage();
                                            if(!info->load(token))
                                            {
                                                qDebug() << "La textura " << token << " no s'ha carregat correctament";
                                                return false;
                                            }
                                            *info = QGLWidget::convertToGLFormat(*info);
                                            i--; //La info és opcional
                                        }else if(strcmp(token, "end") == 0 || strcmp(token, "End") == 0 || strcmp(token, "END") == 0){
                                            //END
                                            if(pintura != NULL && marc != NULL){
                                                quadre = new Quadre(*marc, *pintura, info);
                                                memcpy(quadre->nom, nom, sizeof(quadre->nom)); //a token2 hi teniem el nom
                                                quadre->nom[sizeof(quadre->nom)-1]='\0'; //per si de cas acabem l'string
                                                Quadres.push_back(*quadre);
                                            }else
                                                return false;
                                            construintEntitat = false;
                                            if(*strsep(&aux, " \t\n") != '\0') return false; //Comprovem que no hi hagi res més a la línia
                                        } else {
                                            //no es tracta ni del token Object ni de End
                                            return false;
                                        }
                                    }else{
                                        i--; //si es tracta d'una línia en blanc, la ignorem
                                    }
                                }else{
                                    i--; //si es tracta d'un comentari, ignorem la línia
                                }
                            }else{
                                return false; //si fgets retorna null vol dir que s'ha acabat el fitxer
                            }
                        }
                    } else if(strcmp(token, "character") == 0 || strcmp(token, "Character") == 0 || strcmp(token, "CHARACTER") == 0){
                        //PERSONA
                        Persona* persona = new Persona();
                        bool teCabell = false;
                        bool teCamaDreta = false;
                        bool teCara = false;
                        bool teCamaEsquerra = false;
                        bool teCos = false;

                        if(*(token2 = strsep(&aux, " \t\n")) == '\0') return false; //el nom sempre ha d'existir
                        strcpy(nom, token2);
                        if(*strsep(&aux, " \t\n") != '\0') return false; //Comprovem que no hi hagi res més a la línia
                        //A la següent linia hi haura tots els objectes: cuixa, tros, etc. i despres un end
                        for(int i=0; i<6; i++){ //numero de parts del cos + end
                            if(fgets(line, size, file) != NULL){
                                aux = line; //fem que aux apunti a line
                                while(*aux == '\t' || *aux == ' ') aux++; //treiem les tabulacions o els espais del principi de la linia
                                if(*aux != '#'){ //comentari?
                                    //agafem el primer token
                                    token = strsep(&aux, " \t\n");
                                    if(!(token == NULL || *token == '\0')){ //linia en blanc?
                                        if(strcmp(token, "enable") == 0 || strcmp(token, "Enable") == 0 || strcmp(token, "ENABLE") == 0){
                                            getPropietat(true, aux, &colisions, &brilla);
                                            i--; //aixo no estava previst
                                        }else if(strcmp(token, "disable") == 0 || strcmp(token, "Disable") == 0 || strcmp(token, "DISABLE") == 0){
                                            getPropietat(false, aux, &colisions, &brilla);
                                            i--;
                                        }else if(strcmp(token, "hair") == 0 || strcmp(token, "Hair") == 0 || strcmp(token, "HAIR") == 0){
                                            persona->Cabell = Objecte(importador);
                                            strcpy(persona->Cabell.nom, "cabell");
                                            if(!llegirObjecte(&(persona->Cabell), aux, colisions,brilla))
                                                return false;
                                            teCabell = true;
                                        }else if(strcmp(token, "face") == 0 || strcmp(token, "Face") == 0 || strcmp(token, "FACE") == 0){
                                            persona->Cara = Objecte(importador);
                                            strcpy(persona->Cara.nom, "cara");
                                            if(!llegirObjecte(&(persona->Cara), aux, colisions,brilla))
                                                return false;
                                            teCara = true;
                                        }else if(strcmp(token, "body") == 0 || strcmp(token, "Body") == 0 || strcmp(token, "BODY") == 0){
                                            persona->Cos = Objecte(importador);
                                            strcpy(persona->Cos.nom, "cos");
                                            if(!llegirObjecte(&(persona->Cos), aux, colisions,brilla))
                                                return false;
                                            teCos = true;
                                        }else if(strcmp(token, "left_leg") == 0 || strcmp(token, "Left_leg") == 0 || strcmp(token, "Left_Leg") == 0 || strcmp(token, "LEFT_LEG") == 0){
                                            persona->CamaEsquerra = Objecte(importador);
                                            strcpy(persona->CamaEsquerra.nom, "cos");
                                            if(!llegirObjecte(&(persona->CamaEsquerra), aux, colisions,brilla))
                                                return false;
                                            teCamaEsquerra = true;
                                        }else if(strcmp(token, "right_leg") == 0 || strcmp(token, "Right_leg") == 0 || strcmp(token, "Right_Leg") == 0 || strcmp(token, "RIGHT_LEG") == 0){
                                            persona->CamaDreta = Objecte(importador);
                                            strcpy(persona->CamaDreta.nom, "cos");
                                            if(!llegirObjecte(&(persona->CamaDreta), aux, colisions,brilla))
                                                return false;
                                            teCamaDreta = true;
                                        }else if(strcmp(token, "end") == 0 || strcmp(token, "End") == 0 || strcmp(token, "END") == 0){
                                            //END
                                            personatgeDonat = true;
                                            memcpy(persona->nom, nom, sizeof(persona->nom)); //a token2 hi teniem el nom
                                            persona->nom[sizeof(persona->nom)-1]='\0'; //per si de cas acabem l'string
                                            if(teCara && teCos && teCamaDreta && teCamaEsquerra && teCabell){
                                                //Forcem que no colisioni
                                                persona->Cara.colisiona = false;
                                                persona->Cabell.colisiona = false;
                                                persona->Cos.colisiona = false;
                                                persona->CamaDreta.colisiona = false;
                                                persona->CamaEsquerra.colisiona = false;
                                                Persones.push_back(*persona);
                                            }else
                                                return false;
                                            construintEntitat = false;
                                            if(*strsep(&aux, " \t\n") != '\0') return false; //Comprovem que no hi hagi res més a la línia
                                        } else {
                                            //no es tracta ni del token Object ni de End
                                            return false;
                                        }
                                    }else{
                                        i--; //si es tracta d'una línia en blanc, la ignorem
                                    }
                                }else{
                                    i--; //si es tracta d'un comentari, ignorem la línia
                                }
                            }else{
                                return false; //si fgets retorna null vol dir que s'ha acabat el fitxer
                            }
                        }
                    } else {
                        //No es tracta ni de entity ni de picture ni de object ni de end
                        return false;
                    }
                }
            }
        }
        if(personatgeDonat)
            return true;
        else
            qDebug() << "Falta el personatge";
    }
    return false;
}

bool GLWidget::getPropietat(bool enable, char* buffer, bool* colisions, bool* brilla){
    char* token;

    if(*(token = strsep(&buffer, " \t\n")) == '\0') return false; //el primer token sempre ha d'existir

    if(strcmp(token, "collisions") == 0 || strcmp(token, "Collisions") == 0 || strcmp(token, "COLLISIONS") == 0){
        *colisions = enable;
    }else if(strcmp(token, "brightness") == 0 || strcmp(token, "Brightness") == 0 || strcmp(token, "BRIGHTNESS") == 0){
            *brilla = enable;
    } else {
        qDebug() << "Bad property at Enable/Disable";
        return false;
    }

    return true;
}

bool GLWidget::llegirVector(char* buffer, float* vector, int dim){
    char* token;
    QByteArray str;
    for(int i = 0; i<dim; i++){
        token = strsep(&buffer, " \t\n");
        if(*token == '\0') return false; //si no hi ha res més retornem false
        str = QByteArray(token);
        vector[i] = str.toFloat();
    }
    if(*strsep(&buffer, " \t\n") != '\0') return false; //Comprovem que no hi hagi res més a la línia
    return true;
}

bool GLWidget::llegirObjecte(Objecte* obj, char* buffer, bool colisiona, bool brilla){
    char *token;

    if(*(token = strsep(&buffer, " \t\n")) == '\0') return false; //llegim el path de l'objecte
    if(!obj->load(token)) return false; //carreguem l'objecte
    token = strsep(&buffer, " \t\n"); //llegim el path de la textura
    if(*token == '\0'){
        //No hi ha textura
        obj->hasTexture = false;
    } else {
        //Llegim la textura
        QImage textura, texturaNormals;
        if(!textura.load(token))
        {
            qDebug() << "La textura " << token << " no s'ha carregat correctament";
            return false;
        }
        textura = QGLWidget::convertToGLFormat(textura);

        //Assignem un index de textures d'opengl que no esta sent utilitzat per a la nostra textura
        glGenTextures(1, &(obj->textura));

        //Activem la textura 0
        glActiveTexture(GL_TEXTURE0);

        //Seleccionem l'index de la textura amb la que volem treballar, que sera la nostra variable textura
        glBindTexture(GL_TEXTURE_2D, obj->textura);

        //Li donem a openGL la nostra imatge, que ja la tenim convertida amb el format necessari al buffer texturaGL
        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, textura.width(), textura.height(), GL_RGBA, GL_UNSIGNED_BYTE, textura.bits());

        //Li diem a openGL quins son els parametres d'interpolacio
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

        obj->hasTexture = true;

        //mirem si té textura per bumpmapping
        token = strsep(&buffer, " \t\n");
        if(*token != '\0'){
            if(!texturaNormals.load(token))
            {
                qDebug() << "La textura de normals " << token << " no s'ha carregat correctament";
                return false;
            }
            texturaNormals = QGLWidget::convertToGLFormat(texturaNormals);

            //Assignem un index de textures d'opengl que no esta sent utilitzat per a la nostra textura
            glGenTextures(1, &(obj->texturaNormals));

            //Activem la textura 1 -> la utilitzarem per normals!
            glActiveTexture(GL_TEXTURE1);

            //Seleccionem l'index de la textura amb la que volem treballar, que sera la nostra variable textura
            glBindTexture(GL_TEXTURE_2D, obj->texturaNormals);

            //Li donem a openGL la nostra imatge, que ja la tenim convertida al format necessari al buffer textura
            gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, texturaNormals.width(), texturaNormals.height(), GL_RGBA, GL_UNSIGNED_BYTE, texturaNormals.bits());
            //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texturaNormals.width(), texturaNormals.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texturaNormals.bits());
            //Li diem a openGL quins son els parametres d'interpolacio
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

            obj->hasNormalTexture = true;
            if(*strsep(&buffer, " \t\n") != '\0') return false; //Comprovem que no hi hagi res més a la línia
        }
    }
    //calculem la bounding sphere, que es guardara a l'objecte
    obj->calcularBoundingSphere();
    obj->colisiona = colisiona;
    obj->brillant = brilla;
    obj->generaLlista();
    return true;
}
