#ifndef MAPA_H
#define MAPA_H

#include <vector>

#include "Personatge.h"
#include "Camera.h"
#include "Objecte.h"
#include "Entitat.h"
#include "Quadre.h"
#include "Frustum.h"
#include "Shader.h"

//CONSTANTS
#define PI 3.14159265

//COLISIONS
//MIDA DEL MAPA(CENTRAT EN EL ZERO)
#define COLISIONSW 50
#define COLISIONSH 50
//DESPLAÇAMENT DEL MARGE INFERIOR DRET DEL BUFFER (0,COLISIONSH) RESPECTE DEL CENTRE
#define DESP_COL_X -30
#define DESP_COL_Y -30
/*****************************************************************************
 *                ____________________
 *               |(COLISIONSW,0)      |
 *               |                    |
 *               |                    |
 *               |         +          |COLISIONSH
 *               |                    |
 *               |                    |
 *               |____________________|(0,COLISIONSW)
 *                      COLISIONSW
 *
 *****************************************************************************/
//MIDA DEL CAMP DE MOVIMENTS CENTAT EN L ZERO
#define LIMIT_X 60
#define LIMIT_Y 60
//LLINDAR DELS OBSTACLES (ALTURA A PARTIR DE LA QUAL NO PODEM PASSAR, DE 0 A 1
#define LLINDAR 0



class Mapa
{
public:
    Mapa();
    ~Mapa();
    void generaMapaColisions(std::vector<Entitat> Entitats, std::vector<Quadre> Quadres,Shader* shader);
    void update(Personatge *pers);
    void Dibuixa(Camera *camera, Personatge *pers, GLfloat w, GLfloat h,std::vector<Entitat> Entitats, std::vector<Quadre> Quadres, Shader* shader, std::vector<Personatge> users);
    bool calculaColisions(GLfloat x, GLfloat y);

    bool colisions;
private:
    unsigned char color[3][4];
    GLfloat bufferCol[COLISIONSW][COLISIONSH];    //w*h*profunditat
    Frustum* frustum;
};

#endif // MAPA_H
