#include "Mapa.h"
#include "Camera.h"
#include <iostream>

Mapa::Mapa()
{
    frustum=new Frustum();
    this->colisions=false;
}

Mapa::~Mapa()
{}

void Mapa::generaMapaColisions(std::vector<Entitat> Entitats, std::vector<Quadre> Quadres,Shader* shader){
    GLint i=0, j=0;
    GLint alsada_camera=10.0;
    Camera *camera=new Camera();


    glDisable(GL_LIGHTING);


    glViewport(0,0,COLISIONSW,COLISIONSH);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    camera->setOrthogonal(10,10,32);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    camera->setPlanta();
    glTranslatef(-3.5,0,1);
    frustum->extractFrustum();
    for(unsigned int i=0; i<Entitats.size(); i++){
        Entitats[i].dibuixa(shader,frustum);
    }
    for(unsigned int i=0; i<Quadres.size(); i++){
        Quadres[i].dibuixa(shader,frustum);
    }

    glPushMatrix();
        glTranslatef(10.0,0,0);
        glBegin(GL_POLYGON);
            glVertex3f(10.0f,-10.0f, 10.0f);
            glVertex3f(-10.0f,-10.0f, 10.0f);
            glVertex3f(-10.0f,10.0f, 10.0f);
            glVertex3f(10.0f,10.0f, 10.0f);
        glEnd();
    glPopMatrix();


    //Determinem buffer a llegir (de la pantalla)
    glReadBuffer(GL_BACK);

    // Llegim el pixels que ens interessen
    glReadPixels(0,0,COLISIONSW,COLISIONSH,GL_DEPTH_COMPONENT,GL_FLOAT,this->bufferCol);//GLint x,GLint y,GLsizei width,GLsizei height,GLenum format,GLenum type,GLvoid *data

    /*for(i=0;i<COLISIONSH;i++){
        for(j=0;j<COLISIONSW;j++)
            std::cout<<bufferCol[i][j]<<" ";
    }*/
    glEnable(GL_LIGHTING);
}

void Mapa::Dibuixa(Camera *cam, Personatge *pers,GLfloat w,GLfloat h, std::vector<Entitat> Entitats, std::vector<Quadre> Quadres, Shader* shader, std::vector<Personatge> users){
    GLfloat dist=10.0;
    glDisable(GL_LIGHTING);
    glViewport(2*w/3,2*h/3,w/3,h/3);
    glScissor(2*w/3,2*h/3,w/3,h/3);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    cam->setOrthogonal(20,20,dist);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
        glRotatef(90,0.0f,0.0f,1.0f);
        glBegin(GL_POLYGON);
            glVertex3f(0.75f, 0.0f, 0.0f);
            glVertex3f(-0.75f, -0.5f, 0.0f);
            glVertex3f(0.0f,0.0f, 0.0f);
            glVertex3f(-0.75f,0.5f, 0.0f);
        glEnd();
    glPopMatrix();
    cam->setPlanta();

    glRotatef(pers->orientacioh-90,0.0,1.0,0.0);
    glTranslatef(-pers->x,0.0f,-pers->z);

    frustum->extractFrustum();
    for(unsigned int i=0; i<Entitats.size(); i++){
        Entitats[i].dibuixaSimple(shader, frustum, true);
    }
    for(unsigned int i=0; i<Quadres.size(); i++){
        Quadres[i].dibuixa(shader, frustum, true, true);
    }

    for(std::vector<Personatge>::iterator u = users.begin(); u != users.end(); ++u){
        glPushMatrix();
            glTranslatef(u->x,1,u->z);
            glRotatef(u->orientacioh+90,0.0,1.0,0.0);
            //glColor4f(1.0f,0.0f,0.0f,0.0f);
            glBegin(GL_POLYGON);
                glVertex3f(0.75f, 0.0f, 0.0f);
                glVertex3f(-0.75f, 0.0f, -0.5f);
                glVertex3f(0.0f,0.0f, 0.0f);
                glVertex3f(-0.75f,0.0f, 0.5f);
            glEnd();
        glPopMatrix();
    }

    cam->setPlanta();
    glEnable(GL_LIGHTING);
}

bool Mapa::calculaColisions(GLfloat x, GLfloat y){
    if(this->colisions==false)return true;
    int posx=(x-DESP_COL_X)*COLISIONSW/(LIMIT_X)-2.5;
    int posy=(y-DESP_COL_Y)*COLISIONSH/(LIMIT_Y)+1;
    if(x>LIMIT_X/2||x<-LIMIT_X/2||y>LIMIT_Y/2||y<-LIMIT_Y/2) return false;
    //qDebug()<<"Pos en el mapa:"<<x<<y;
    //qDebug()<<"Pos en el buffer:"<<posx<<" "<<posy<<":"<<this->bufferCol[posx][posy];
    if(this->bufferCol[posx][posy]<LLINDAR) return false;
    return true;
}
