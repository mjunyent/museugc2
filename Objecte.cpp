#include "Objecte.h"

Objecte::Objecte(Assimp::Importer* imp)
{
    importador = imp;
    radi = 0.0;
    centre[0] = 0.0;
    centre[1] = 0.0;
    centre[2] = 0.0;
    colisiona = false;
    brillant = false;
    hasTexture = false;
    hasNormalTexture = false;
    teLlista = false;
}

Objecte::Objecte()
{
    importador = NULL;
    radi = 0.0;
    centre[0] = 0.0;
    centre[1] = 0.0;
    centre[2] = 0.0;
    colisiona = false;
    brillant = false;
    hasTexture = false;
    hasNormalTexture = false;
    teLlista = false;
}

Objecte::~Objecte()
{
}

void Objecte::dibuixa(bool exigimColisio)
{
    int i,j;
    bool continua = true;
    Cara auxc;
    Vertex auxv;
    if(exigimColisio){
        if(!colisiona)
            continua = false;
    }
    if(continua){
        if(teLlista){
            glCallList(llista);
        }else{
            if(this->hasTexture){ //copiem el codi dos cops ja que és molt més eficient estalviar-nos tots els ifs de dins del for
                //Assignem la textura corresponent
                glBindTexture(GL_TEXTURE_2D, this->textura);

                for (i=0;i<cara.size();i++)
                {
                    auxc = this->cara[i];
                    //Pintem cares
                    glBegin(GL_POLYGON);
                    for (j=0;j<auxc.vertex.size();j++)
                    {
                        //Pintem vertexs
                        auxv=auxc.vertex[j];
                        glNormal3f(auxv.nx,auxv.ny,auxv.nz);
                        glTexCoord2f(auxv.s,auxv.t);
                        glVertex3f(auxv.x,auxv.y,auxv.z);
                    }
                    glEnd();
                }
            } else {
                //No tenim textures
                for (i=0;i<cara.size();i++)
                {
                    auxc = this->cara[i];
                    //Pintem cares
                    glBegin(GL_POLYGON);
                    for (j=0;j<auxc.vertex.size();j++)
                    {
                        //Pintem vertexs
                        auxv=auxc.vertex[j];
                        glNormal3f(auxv.nx,auxv.ny,auxv.nz);
                        glVertex3f(auxv.x,auxv.y,auxv.z);
                    }
                    glEnd();
                }
            }
        }
    }
}

void Objecte::dibuixaBump(QGLShaderProgram* bumpShader, int tangentLocation, int bitangentLocation)
{
    int i,j;
    Cara auxc;
    Vertex auxv;

    if(this->hasTexture && this->hasNormalTexture){ //copiem el codi dos cops ja que és molt més eficient estalviar-nos tots els ifs de dins del for
        //Assignem la textura corresponent
        glBindTexture(GL_TEXTURE_2D, this->textura);

        for (i=0;i<cara.size();i++)
        {
            auxc = this->cara[i];
            //Pintem cares
            glBegin(GL_POLYGON);
            for (j=0;j<auxc.vertex.size();j++)
            {
                auxv=auxc.vertex[j];
                //Assignem els atributs al shader
                bumpShader->setAttributeValue(tangentLocation, auxv.tx, auxv.ty, auxv.tz);
                bumpShader->setAttributeValue(bitangentLocation, auxv.bx, auxv.by, auxv.bz);
                bumpShader->setUniformValue("normalMap", 1);
                //Pintem vertexs
                glNormal3f(auxv.nx,auxv.ny,auxv.nz);
                glTexCoord2f(auxv.s,auxv.t);
                glVertex3f(auxv.x,auxv.y,auxv.z);
            }
            glEnd();
        }
    }
}

void Objecte::generaLlista(){
    int i,j;
    Cara auxc;
    Vertex auxv;

    if(!teLlista){
        teLlista = true;
        llista = glGenLists(1);
        glNewList(llista,GL_COMPILE);
            if(this->hasTexture){ //copiem el codi dos cops ja que és molt més eficient estalviar-nos tots els ifs de dins del for
                //Assignem la textura corresponent
                glBindTexture(GL_TEXTURE_2D, this->textura);

                for (i=0;i<cara.size();i++)
                {
                    auxc = this->cara[i];
                    //Pintem cares
                    glBegin(GL_POLYGON);
                    for (j=0;j<auxc.vertex.size();j++)
                    {
                        //Pintem vertexs
                        auxv=auxc.vertex[j];
                        glNormal3f(auxv.nx,auxv.ny,auxv.nz);
                        glTexCoord2f(auxv.s,auxv.t);
                        glVertex3f(auxv.x,auxv.y,auxv.z);
                    }
                    glEnd();
                }
            } else {
                //No tenim textures
                for (i=0;i<cara.size();i++)
                {
                    auxc = this->cara[i];
                    //Pintem cares
                    glBegin(GL_POLYGON);
                    for (j=0;j<auxc.vertex.size();j++)
                    {
                        //Pintem vertexs
                        auxv=auxc.vertex[j];
                        glNormal3f(auxv.nx,auxv.ny,auxv.nz);
                        glVertex3f(auxv.x,auxv.y,auxv.z);
                    }
                    glEnd();
                }
            }
        glEndList();
    }
}

bool Objecte::load(char* FileName)
{
    if(importador != NULL){
        //Llegim l'objecte
        const aiScene* pScene = importador->ReadFile(FileName,
                                                  aiProcess_Triangulate |
                                                  aiProcess_GenSmoothNormals |
                                                  aiProcess_CalcTangentSpace);
        //aiProcess_Triangulate, which translate models that are made from non triangle polygons into triangle based meshes.
        //aiProcess_GenSmoothNormals, generates vertex normals in the case that the original model does not already contain them.
        //aiProcess_FlipUVsv, flips the texture coordinates along the Y axis.

        this->hasTexture = true; //ho posem a true, en cas que no n'hi hagi es posarà a false.

        if (pScene != NULL){
            if(pScene->HasMeshes()){
                //Per cada malla
                for (int i=0; i<pScene->mNumMeshes; i++){
                    aiMesh* mallaActual = pScene->mMeshes[i];
                    if(mallaActual->HasFaces()){
                        //reservem espai al vector cares perquè no hagi d'anar reservant espai (fent mallocs) constantment
                        //d'aquesta manera ens assegurem que el vector tindrà com a mínim capacitat per cares.size + mNumVertices
                        cara.reserve(cara.size()+mallaActual->mNumVertices);
                        if (mallaActual->HasTextureCoords(0)){
                            //Per cada cara
                            for (int j=0; j<mallaActual->mNumFaces; j++){
                                Cara c;
                                aiFace* caraActual = &(mallaActual->mFaces[j]);
                                //reservem espai per a mNumIndices vertexs
                                c.vertex.reserve(caraActual->mNumIndices);
                                //Per cada vertex de la cara (index al vertex)
                                for (int k=0; k<caraActual->mNumIndices; k++){
                                    unsigned int indexActual = caraActual->mIndices[k];
                                    c.vertex.push_back(Vertex(mallaActual->mVertices[indexActual].x,
                                                              mallaActual->mVertices[indexActual].y,
                                                              mallaActual->mVertices[indexActual].z,
                                                              mallaActual->mNormals[indexActual].x,
                                                              mallaActual->mNormals[indexActual].y,
                                                              mallaActual->mNormals[indexActual].z,
                                                              mallaActual->mTextureCoords[0][indexActual].x,
                                                              mallaActual->mTextureCoords[0][indexActual].y,
                                                              mallaActual->mTangents[indexActual].x,
                                                              mallaActual->mTangents[indexActual].y,
                                                              mallaActual->mTangents[indexActual].z,
                                                              mallaActual->mBitangents[indexActual].x,
                                                              mallaActual->mBitangents[indexActual].y,
                                                              mallaActual->mBitangents[indexActual].z
                                                              ));
                                }
                                cara.push_back(c);
                            }
                        } else {
                            this->hasTexture = false;
                            //fem el mateix però no assignem coordenades de textura
                            //Per cada cara
                            for (int j=0; j<mallaActual->mNumFaces; j++){
                                Cara c;
                                aiFace* caraActual = &(mallaActual->mFaces[j]);
                                //reservem espai per a mNumIndices vertexs
                                c.vertex.reserve(mallaActual->mFaces[j].mNumIndices);
                                //Per cada vertex de la cara (index al vertex)
                                for (int k=0; k<mallaActual->mFaces[j].mNumIndices; k++){
                                    unsigned int indexActual = caraActual->mIndices[k];
                                    c.vertex.push_back(Vertex(mallaActual->mVertices[indexActual].x,
                                                              mallaActual->mVertices[indexActual].y,
                                                              mallaActual->mVertices[indexActual].z,
                                                              mallaActual->mNormals[indexActual].x,
                                                              mallaActual->mNormals[indexActual].y,
                                                              mallaActual->mNormals[indexActual].z,
                                                              -1.0,
                                                              -1.0));
                                }
                                cara.push_back(c);
                            }
                        }
                    }
                }
                return true;
            }
        }
    }
    return false;
}

void Objecte::calcularBoundingSphere(){
    float sum[3];
    float actual;
    int n = 0;
    float x, y, z;

    radi=0;

    sum[0] = 0;
    sum[1] = 0;
    sum[2] = 0;
    for(int i=0; i<cara.size(); i++){
        for(int j=0; j<cara.at(i).vertex.size(); j++){
            sum[0]+=cara[i].vertex[j].x;
            sum[1]+=cara[i].vertex[j].y;
            sum[2]+=cara[i].vertex[j].z;
            n++;
        }
    }
    centre[0] = sum[0]/n;
    centre[1] = sum[1]/n;
    centre[2] = sum[2]/n;

    for(int i=0; i<cara.size(); i++){
        for(int j=0; j<cara.at(i).vertex.size(); j++){
            Vertex v = cara[i].vertex[j]; //Fem un copia del vertex per treballar-hi al for
            x = v.x - centre[0];
            y = v.y - centre[1];
            z = v.z - centre[2];
            actual = x*x+y*y+z*z;
            if(radi < actual){
                radi = actual;
            }
        }
    }
    radi = sqrt(radi);
}
