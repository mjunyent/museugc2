#ifndef FRUSTUM_H
#define FRUSTUM_H
#include "Util.h"
class Frustum
{
public:
    Frustum();
    bool pointInFrustum(float x, float y, float z);
    int sphereInFrustum(float x, float y, float z, float radius);
    void extractFrustum();
private:
    float frustum[6][4];
};

#endif // FRUSTUM_H
