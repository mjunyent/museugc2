#ifndef QUADRE_H
#define QUADRE_H

#include "Objecte.h"
#include "QImage"
#include "Frustum.h"
#include "Shader.h"

#define PICTURE_NAME_LENGTH 32

class Quadre
{
public:
    Quadre(Objecte m, Objecte p, QImage *inf);
    Objecte marc;
    Objecte pintura;
    void dibuixa(Shader *shader, Frustum *frustum, bool simple = false, bool exigimColisions = false);
    void showInfo(int w, int h);
    char nom[PICTURE_NAME_LENGTH];
    float normal[3];
    float centre[3];
    bool hasInfo;
private:
    int infow;
    int infoh;
    QImage info;
};

#endif // QUADRE_H
