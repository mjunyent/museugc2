QT += core \
    gui \
    opengl
unix:!macx:LIBS += -lglut \
    -lGLEW \
    -lGLU \
    -lassimp
win32:LIBS += "C:\Archivos de programa\Microsoft SDKs\Windows\v6.1\Lib\glew32.lib"
macx:LIBS += -framework \
    GLUT
TARGET = Museu
TEMPLATE = app
SOURCES += main.cpp \
    GLWidget.cpp \
    Camera.cpp \
    Personatge.cpp \
    museuwindow.cpp \
    Client.cpp \
    Objecte.cpp \
    Mapa.cpp \
    Entitat.cpp \
    Llum.cpp \
    Quadre.cpp \
    Persona.cpp \
    Frustum.cpp \
    Shader.cpp
HEADERS += museuwindow.h \
    Util.h \
    GLWidget.h \
    Camera.h \
    Personatge.h \
    Common.h \
    Client.h \
    Objecte.h \
    Mapa.h \
    Entitat.h \
    Llum.h \
    Quadre.h \
    Persona.h \
    Frustum.h \
    Shader.h
FORMS += museuwindow.ui
OTHER_FILES += \
	museu.mu \
    shaders/simple.vert \
    shaders/simple.frag \
    shaders/phong.vert \
    shaders/phong.frag \
    shaders/gouraud.vert \
    shaders/gouraud.frag \
    shaders/bumpMapping.vert \
    shaders/bumpMapping.frag
