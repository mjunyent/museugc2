#include "Persona.h"
#include <QElapsedTimer>

Persona::Persona()
{
    primerCop=true;
}

void Persona::dibuixa(float x, float y, float z, float angleh, float angleE, float angleD){
    if (primerCop){
        this->CamaEsquerra.calcularBoundingSphere();
        this->CamaDreta.calcularBoundingSphere();
        centreE[0] = CamaEsquerra.centre[0];
        centreE[1] = CamaEsquerra.centre[1];
        centreE[2] = CamaEsquerra.centre[2];
        centreD[0] = CamaDreta.centre[0];
        centreD[1] = CamaDreta.centre[1];
        centreD[2] = CamaDreta.centre[2];
        radiE = CamaEsquerra.radi;
        radiD = CamaDreta.radi;
    }
    glPushMatrix();
    glTranslatef(x, y, z);
    glRotatef(-angleh, 0.0, 1.0, 0.0);
    glPushMatrix();
    glTranslatef(centreD[0],centreD[1]+radiD,centreD[2]);
    glRotatef(angleD,1.0,0.0,0.0);
    glTranslatef(-centreD[0],-centreD[1]-radiD,-centreD[2]);
    CamaDreta.dibuixa();
    glPopMatrix();
    glPushMatrix();
    glTranslatef(centreE[0],centreE[1]+radiE,centreE[2]);
    glRotatef(angleE,1.0,0.0,0.0);
    glTranslatef(-centreE[0],-centreE[1]-radiE,-centreE[2]);
    CamaEsquerra.dibuixa();
    glPopMatrix();
    Cara.dibuixa();
    Cos.dibuixa();
    Cabell.dibuixa();
    glPopMatrix();
}
