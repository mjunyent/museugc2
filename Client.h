#ifndef CLIENT_H
#define CLIENT_H

#include <stdlib.h>
#include <string.h>
#include "Common.h"
#include "Personatge.h"
#include "Persona.h"
#include <vector>
#include <QString>
#include <pthread.h>
#include <QObject>
#include <fcntl.h> //per poder posar el socket a non-blocking mode

#define SEC_MAX_REBRE 1
#define USEC_MAX_REBRE 0
#define SEC_MAX_CONNECTA 5

class Client : public QObject
{
    Q_OBJECT
public:
    Client(QObject *parent, Personatge *personatge);
    ~Client();
    bool connecta(char *serverIP, unsigned short serverPort);
    void desconnecta();
    bool logIn(char *nom);
    bool logOut();
    bool enviarMissatgePrivat(char *missatge, int toID);
    bool enviarMissatgeGlobal(char *missatge);
    bool descartaBrosaRebuda();
    bool rebreNovetats(std::vector<Persona> *persones);
    void funcioRebreNovetats();
    void processaMissatges();
    void dibuixaUsuaris();
    bool enviaPosicio();
    bool connectat;
    bool loggedIn;
    bool rebentNovetats;
    int numUsers;
    std::vector<Personatge> users;
    std::vector<message> missatges;
    std::vector<Persona>* persones;

signals:
    void nouMissatge(QString nom, QString missatge, bool privat);

private:
    bool rebre(char* bufferRebre, int length);
    bool enviarMissatge(char *missatge, int toID);

    int sock;
    int id;
    Personatge* pers;
    struct sockaddr_in serverAddr;
    pthread_t thread;
};

#endif // CLIENT_H
