#ifndef ENTITAT_H
#define ENTITAT_H

#include <vector>
#include "Objecte.h"
#include "Frustum.h"
#include "Shader.h"

#define ENTITY_NAME_LENGTH 32

class Entitat
{
public:
    Entitat();

    void dibuixa(Shader *shader, Frustum *frustum);
    void dibuixaSimple(Shader* shader, Frustum* frustum, bool exigimColisions);
    std::vector<Objecte> objecte;
    char nom[ENTITY_NAME_LENGTH];
};

#endif // ENTITAT_H
