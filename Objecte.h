#ifndef OBJECTE_H
#define OBJECTE_H
#include <vector>
#include <string>
#include <assimp/include/assimp/Importer.hpp>
#include <assimp/include/assimp/scene.h>
#include <assimp/include/assimp/postprocess.h>
#include "Util.h"
#include <QGLShaderProgram>

#define TEXTURE_LENGTH 50
#define OBJECT_NAME_LENGTH 32

class Objecte
{
    public:
        class Vertex{
            public:
                float x; //coordenades del vertex
                float y;
                float z;
                float nx; //coordenades de la normal
                float ny;
                float nz;
                float s;
                float t; //coordenades de textura
                float tx;
                float ty;
                float tz;
                float bx;
                float by;
                float bz;

                Vertex() {}
                Vertex(float _x, float _y, float _z, float _nx, float _ny, float _nz, float _s, float _t)
                    : x(_x), y(_y), z(_z), nx(_nx), ny(_ny), nz(_nz), s(_s), t(_t)
                {}
                Vertex(float _x, float _y, float _z, float _nx, float _ny, float _nz, float _s, float _t, float _tx, float _ty, float _tz, float _bx, float _by, float _bz)
                    : x(_x), y(_y), z(_z), nx(_nx), ny(_ny), nz(_nz), s(_s), t(_t), tx(_tx), ty(_ty), tz(_tz), bx(_bx), by(_by), bz(_bz)
                {}

        };

        class Cara{
            public:
                Cara() {}
                std::vector<Vertex> vertex;
        };

        float radi;
        float centre[3];
        Objecte(Assimp::Importer* imp);
        Objecte();
        ~Objecte();
        void dibuixa(bool exigimColisio = false);
        void dibuixaBump(QGLShaderProgram *bumpShader, int tangentLocation, int bitangentLocation);
        void generaLlista();
        void calcularBoundingSphere();
        bool load(char *FileName);
        char nom[OBJECT_NAME_LENGTH];
        std::vector<Cara> cara;
        bool hasTexture;
        bool hasNormalTexture;
        GLuint textura;
        GLuint texturaNormals;
        bool colisiona;
        bool brillant;
        bool teLlista;
        GLuint llista;
    private:
        Assimp::Importer* importador;
};

#endif // OBJECTE_H
