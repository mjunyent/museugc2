#include "Llum.h"
#include <iostream>
Llum::Llum(float pos[4], float dif[4], float amb[4], float spe[4], int index)
{
    //Ens guardem els paràmetres del llum
    for (int i=0; i<4; i++){
        this->pos[i] = pos[i];
        this->dif[i] = dif[i];
        this->amb[i] = amb[i];
        this->spe[i] = spe[i];
    }

    //Per a tenir compatibilitat amb opengl 1.0
    switch(index){
    case 0:
        this->index = GL_LIGHT0;
        break;
    case 1:
        this->index = GL_LIGHT1;
        break;
    case 2:
        this->index = GL_LIGHT2;
        break;
    case 3:
        this->index = GL_LIGHT3;
        break;
    case 4:
        this->index = GL_LIGHT4;
        break;
    case 5:
        this->index = GL_LIGHT5;
        break;
    case 6:
        this->index = GL_LIGHT6;
        break;
    case 7:
        this->index = GL_LIGHT7;
        break;
    default:
        this->index = -1;
        break;
    }

    if(this->index != -1){
        //Li diem a opengl els paràmetres del llum
        glLightfv(this->index,GL_POSITION, pos);
        glLightfv(this->index,GL_DIFFUSE,  dif);
        glLightfv(this->index,GL_SPECULAR, spe);
        glLightfv(this->index,GL_AMBIENT,  amb);
        glEnable(this->index);
    }
}

/*
    El que hi havia a Illumination.cpp

            glShadeModel(GL_SMOOTH);
            glEnable(GL_LIGHTING);
            glEnable(GL_COLOR_MATERIAL);

*/
