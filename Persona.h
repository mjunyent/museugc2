#ifndef PERSONA_H
#define PERSONA_H
#include <QElapsedTimer>
#include "Objecte.h"
#define PERSONA_NAME_LENGTH 32
#define PASSA 50.0 //una pasa son 70 graus de moviment
#define PASSAM 2.5 //una pasa son 5 unitats
class Persona
{
public:
    Persona();
    Objecte Cabell;
    Objecte Cara;
    Objecte Cos;
    Objecte CamaDreta;
    Objecte CamaEsquerra;
    char nom[PERSONA_NAME_LENGTH];
    float centreE[3];
    float centreD[3];
    float radiE;
    float radiD;
    float primerCop;
    QElapsedTimer timer;
    QElapsedTimer timer2;
    void dibuixa(float x, float y, float z, float angleh, float angleD, float angleE);
};

#endif // PERSONA_H
