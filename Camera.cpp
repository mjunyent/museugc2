#include "Camera.h"

Camera::Camera()
{
    // Initialize Modelview and Projection matrixes
    // (to set a clean state, even though this should be the default OpenGL values)
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Set default values for camera and projection
    near   = .1;
    far    = 500;
    alpha = 45.0;
}

Camera::~Camera()
{
}

void Camera::setProjection(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // Projection will be adapted to inverse aspect ratios
    if (w>=h)
        gluPerspective(alpha,1.0*w/h,near,far);
    else
        gluPerspective(alpha*h/w,1.0*w/h,near,far);
    glMatrixMode(GL_MODELVIEW);
}

void Camera::setOrthogonal(int w, int h, int R)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Projection will be adapted to inverse aspect ratios
    if (w>=h)
        glOrtho(-R*(w/h),R*(w/h),-R,R,0,R*2);
    else
        glOrtho(-R,R,-R*(h/w),R*(h/w),0,R*2);
    glMatrixMode(GL_MODELVIEW);
}

/*void Camera::setAlsat()
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glRotatef(pers->orientaciov,1.0f,0.0f,0.0f);
    glRotatef(pers->orientacioh,0.0f,1.0f,0.0f);
    glTranslatef(pers->x,-pers->y,pers->z);

}*/

void Camera::setPlanta()
{
    gluLookAt(0.0,1.0,0.0,
              0.0,0.0,0.0,
              1.0,0.0,0.0);
}

void Camera::update(Personatge *pers, float dist)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0,0,-dist);
    glRotatef(pers->orientaciov,1.0f,0.0f,0.0f);
    glRotatef(pers->orientacioh,0.0f,1.0f,0.0f);
    glTranslatef(-pers->x,-pers->y,-pers->z);
}
