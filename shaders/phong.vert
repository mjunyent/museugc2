uniform sampler2D colorMap;
varying vec4 posicioVertex;
varying vec3 normal;

void main()
{
    posicioVertex = gl_ModelViewMatrix * gl_Vertex;       //Traslladem, rotem i escalem els vertex i els passem a coord. punt de vista
    normal = gl_NormalMatrix*gl_Normal;                     //Expressem les normals en coord. punt de vista

    gl_Position = ftransform();
    gl_FrontColor = gl_Color;

    //posem al varying gl_TexCoord[0] les coordenades de la textura
    gl_TexCoord[0] = gl_MultiTexCoord0;
}
