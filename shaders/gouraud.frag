uniform sampler2D colorMap;
varying vec3 intensitat;

void main()
{
    gl_FragColor = texture2D(colorMap, gl_TexCoord[0].st)*vec4(intensitat,1.0);
}
