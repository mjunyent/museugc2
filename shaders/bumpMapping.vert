uniform sampler2D colorMap;
uniform sampler2D normalMap;
attribute vec3 tangent;
attribute vec3 bitangent;
varying vec4 posicioVertex;
varying vec3 dirLlum[6];

void main()
{
    //Creem la matriu canvi de base
    //passem els vector de món a punt de vista
    vec3 n = normalize(gl_NormalMatrix * gl_Normal);
    vec3 t = normalize(gl_NormalMatrix * tangent);
    vec3 b = normalize(gl_NormalMatrix * bitangent);

    mat3 canviBaseEspaiTangent; //matriu que canvia de punt de vista a espai tangent
    canviBaseEspaiTangent[0][0] = t.x;
    canviBaseEspaiTangent[1][0] = t.y;
    canviBaseEspaiTangent[2][0] = t.z;
    canviBaseEspaiTangent[0][1] = b.x;
    canviBaseEspaiTangent[1][1] = b.y;
    canviBaseEspaiTangent[2][1] = b.z;
    canviBaseEspaiTangent[0][2] = n.x;
    canviBaseEspaiTangent[1][2] = n.y;
    canviBaseEspaiTangent[2][2] = n.z;

    //passem la posicio a coord punt de vista
    posicioVertex = gl_ModelViewMatrix * gl_Vertex;

    //calculem la direccio de la llum, en coordenades punt de vista i la passem la a coord espai tangent
    for(int i=0; i<6; i++)
        dirLlum[i].xyz = canviBaseEspaiTangent * (gl_LightSource[i].position.xyz - posicioVertex.xyz);

    //passem la posicio a coord espai tangent
    posicioVertex.xyz = canviBaseEspaiTangent * posicioVertex.xyz;

    //ProjectionMatrix * ModelViewMatrix * gl_Vertex
    gl_Position = ftransform();

    //posem al varying gl_TexCoord[0] les coordenades de la textura
    gl_TexCoord[0] = gl_MultiTexCoord0;

    //no cal posar unes altres coordenades per a les normals
    //gl_TexCoord[1] = gl_MultiTexCoord1;
}
