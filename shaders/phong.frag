uniform sampler2D colorMap;
varying vec4 posicioVertex;
varying vec3 normal;

vec3 calcularIntensitat(vec3 normal, vec3 posicioVertex){
    vec3 resultat;
    float cosAngle;
    vec3 direccioLlum;
    vec3 reflexe;

    resultat = vec3(0.0);

    for(int i=0; i<6; i++){
        //AMBIENT
        resultat.xyz+=gl_LightSource[i].ambient.xyz;

        direccioLlum.xyz = gl_LightSource[i].position.xyz - posicioVertex.xyz;

        //DIFUSA
        cosAngle = dot(normalize(normal.xyz), normalize(direccioLlum.xyz));

        //atencio, quan tinguem materials hi haura un coeficient de reflexió difusa multiplicant aixo:
        resultat.xyz += max(vec3(0.0),cosAngle*gl_LightSource[i].diffuse.xyz);

        //ESPECULAR
        reflexe = -reflect(direccioLlum.xyz, normal.xyz);
        cosAngle = max(0.0,dot(normalize(reflexe),-normalize(posicioVertex)));
        resultat.xyz += /*gl_FrontMaterial.specular.xyz **/ gl_LightSource[i].specular.xyz * vec3(pow(cosAngle, 250.0/*gl_FrontMaterial.shininess*/));
    }
    return resultat;
}

void main()
{
    gl_FragColor = texture2D(colorMap, gl_TexCoord[0].st)*vec4(calcularIntensitat(normal.xyz, posicioVertex.xyz),1.0);
}
