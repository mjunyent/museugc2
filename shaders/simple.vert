uniform sampler2D colorMap;
void main()
{
    vec2 coordText = vec2(0.0, 0.0);
    gl_FrontColor = texture2D(colorMap, coordText); //per utilitzar més d'un color, canviar coordText per gl_MultiTexCoord0.st
    gl_Position = ftransform();
}
