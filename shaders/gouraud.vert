uniform sampler2D colorMap;
varying vec3 intensitat;

vec3 calcularIntensitat(vec3 normal, vec3 posicioVertex){
    vec3 resultat;
    float cosAngle;
    vec3 direccioLlum;
    vec3 reflexe;

    resultat = vec3(0.0);

    for(int i=0; i<6; i++){
        //AMBIENT
        resultat.xyz+=gl_LightSource[i].ambient.xyz;

        direccioLlum.xyz = gl_LightSource[i].position.xyz - posicioVertex.xyz;

        //DIFUSA
        cosAngle = dot(normalize(normal.xyz), normalize(direccioLlum.xyz));

        //atencio, quan tinguem materials hi haura un coeficient de reflexió difusa multiplicant aixo:
        resultat.xyz += max(vec3(0.0),cosAngle*gl_LightSource[i].diffuse.xyz);

        //ESPECULAR
        reflexe = -reflect(direccioLlum.xyz, normal.xyz);
        cosAngle = max(0.0,dot(normalize(reflexe),-normalize(posicioVertex)));
        resultat.xyz += /*gl_FrontMaterial.specular.xyz **/ gl_LightSource[i].specular.xyz * vec3(pow(cosAngle, 250.0/*gl_FrontMaterial.shininess*/));
    }
    return resultat;
}

void main()
{
    vec3 normal;
    vec4 posicioVertex;

    posicioVertex = gl_ModelViewMatrix * gl_Vertex;       //Traslladem, rotem i escalem els vertex i els passem a coord. punt de vista
    normal = gl_NormalMatrix*gl_Normal;                     //Expressem les normals en coord. punt de vista
    intensitat = calcularIntensitat(normal, posicioVertex.xyz);

    gl_Position = ftransform();

    gl_FrontColor = gl_Color;

    //posem al varying gl_TexCoord[0] les coordenades de la textura
    gl_TexCoord[0] = gl_MultiTexCoord0;
}
