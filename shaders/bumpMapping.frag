uniform sampler2D colorMap;
uniform sampler2D normalMap;
varying vec4 posicioVertex;
varying vec3 dirLlum[6];

vec3 calcularIntensitat(vec3 normal, vec3 posicioVertex){
    vec3 resultat;
    float cosAngle;
    vec3 direccioLlum;
    vec3 reflexe;

    resultat = vec3(0.0);

    for(int i=0; i<6; i++){
        //AMBIENT
        resultat.xyz+=gl_LightSource[i].ambient.xyz;

        //DIFUSA
        cosAngle = dot(normalize(normal.xyz), normalize(dirLlum[i].xyz));

        //atencio, quan tinguem materials hi haura un coeficient de reflexió difusa multiplicant aixo:
        resultat.xyz += max(vec3(0.0),cosAngle*gl_LightSource[i].diffuse.xyz);

        //ESPECULAR
        reflexe = -reflect(dirLlum[i].xyz, normal.xyz);
        cosAngle = max(0.0,dot(normalize(reflexe),-normalize(posicioVertex)));
        resultat.xyz += /*gl_FrontMaterial.specular.xyz **/ gl_LightSource[i].specular.xyz * vec3(pow(cosAngle, 250.0/*gl_FrontMaterial.shininess*/));
    }
    return resultat;
}

void main()
{
    vec4 normalBM;
    normalBM = texture2D(normalMap, gl_TexCoord[0].st);
    //0 - 0.5 per normals negatives, 0.5 és el 0, 0.5 - 1 per normals positives
    normalBM.x = normalBM.x*2.0 - 1.0;
    normalBM.y = normalBM.y*2.0 - 1.0;
    normalBM = normalize(normalBM); //potser no cal
    gl_FragColor = texture2D(colorMap   , gl_TexCoord[0].st)*vec4(calcularIntensitat(normalBM.xyz, posicioVertex.xyz),1.0);
}
