#ifndef COMMON_H
#define COMMON_H

#define DEFAULT_PORT 8888
#define USERNAME_LENGTH 20 //també s'ha de canviar a client.h
#define MESSAGE_LENGTH 1000
#define BUFFER_SIZE 1024
#define MAX_USERS 10

#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() and type socklen_t */
#include <netdb.h>      /* for gethostbyname() */
#include <stdio.h>      /* for perror() */
#include <pthread.h>

/*
#include <errno.h> //per gestionar errors
#include <sys/types.h> //per tipus com el uint
*/

//CONSTANTS
#define PI 3.14159265

//OPERATION CODES:
#define LOGIN 'I'
#define LOGOUT 'O'
#define ACCEPT 'A'
#define DENY 'D'
#define MESSAGE 'M'
#define USER 'U'

typedef struct structMessage{
    int fromID;
    int toID; //si es missatge privat posarem la id del destinatari, si és per tothom posarem un -1;
    char content[MESSAGE_LENGTH];
} message;

typedef struct structUser{ //estructura per enviar la informació de nous usuaris del servidor al client
    int id;
    char username[USERNAME_LENGTH];
    float x;
    float y;
    float z;
    float orientation;
} user;

#endif // COMMON_H
