#include "Shader.h"

Shader::Shader(bool readyOpenGL2)
{
    gouraud =  new QGLShaderProgram();
    phong =  new QGLShaderProgram();
    simple = new QGLShaderProgram();
    bump = new QGLShaderProgram();
    openGL2Ready = readyOpenGL2;
    actiu = false;
}

Shader::~Shader(){
    if(gouraud)
        delete gouraud;
    if(phong)
        delete phong;
    if(simple)
        delete simple;
    if(bump)
        delete bump;
}

void Shader::eliminaShaders()
{
    gouraud->removeAllShaders();
    phong->removeAllShaders();
    simple->removeAllShaders();
    bump->removeAllShaders();
}


/*****************************************************************************
 * initializeShaders()
 *      Loads custom shader by specifying filename (expects frag/vert pair)
 *****************************************************************************/
bool Shader::initializeShaders(QGLShaderProgram* sha, QString filename)
{
    if ( !sha->addShaderFromSourceFile( QGLShader::Vertex, filename + ".vert" ) )
        return false;

    if ( !sha->addShaderFromSourceFile( QGLShader::Fragment, filename + ".frag" ) )
        return false;

    if ( !sha->link() )
        return false;

    return true;
}

bool Shader::inicialitzaShaders(){
    bool res = true;
    if(openGL2Ready == true){
        openGL2Ready = false;
        if(!initializeShaders(gouraud, QString("shaders/gouraud")))
            res = false;
        else if(!initializeShaders(phong, QString("shaders/phong")))
            res = false;
        else if(!initializeShaders(simple, QString("shaders/simple")))
            res = false;
        else if(!initializeShaders(bump, QString("shaders/bumpMapping"))){
            res = false;
        }
        if(res){
            openGL2Ready = true;
            tangentLoc = bump->attributeLocation("tangent");
            bitangentLoc = bump->attributeLocation("bitangent");
        } else {
            openGL2Ready = false;
            this->eliminaShaders();
        }
        return res;
    }else
        return false;
}

bool Shader::activaPhong(){
    if(openGL2Ready && actiu){
        phong->bind();
        return true;
    }
    return false;
}

bool Shader::activaGouraud(){
    if(openGL2Ready && actiu){
        gouraud->bind();
        return true;
    }
    return false;
}

bool Shader::activaSimple(){
    if(openGL2Ready && actiu){
        simple->bind();
        return true;
    }
    return false;
}

bool Shader::activaBumpMapping(){
    if(openGL2Ready && actiu){
        bump->bind();
        return true;
    }
    return false;
}
