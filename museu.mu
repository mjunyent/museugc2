#CONFIGURACIÓ MUSEU

#Activem colisions
ENABLE COLLISIONS

#PERSONATGE

#És l'unica cosa que s'ha de donar de forma obligada, pot no haver-hi museu, però hi ha d'haver com a mínim un personatge
Character Noi
    Hair models/Personatges/Noi/Cabells.obj models/Personatges/Noi/Imatges/hair1noCulling.JPG
    Body models/Personatges/Noi/Cos.obj models/Personatges/Noi/Imatges/Material__1noCulling.JPG
    Face models/Personatges/Noi/Cara.obj models/Personatges/Noi/Imatges/hrad1noCulling.JPG
    Left_leg models/Personatges/Noi/CamaEsquerra.obj models/Personatges/Noi/Imatges/Material__1noCulling.JPG
    Right_leg models/Personatges/Noi/CamaDreta.obj models/Personatges/Noi/Imatges/Material__1noCulling.JPG
End

Character Noia
    Hair models/Personatges/Noia/Cabells.obj models/Personatges/Noia/Images/_01_-_Default1noCulling.JPG
    Body models/Personatges/Noia/Cos.obj models/Personatges/Noia/Images/_03_-_Default1noCulling.JPG
    Face models/Personatges/Noia/Cara.obj models/Personatges/Noia/Images/_02_-_Default1noCulling.JPG
    Left_leg models/Personatges/Noia/CamaEsquerra.obj models/Personatges/Noia/Images/_03_-_Default1noCulling.JPG
    Right_leg models/Personatges/Noia/CamaDreta.obj models/Personatges/Noia/Images/_03_-_Default1noCulling.JPG
end

ENABLE BRIGHTNESS

Character MazingerZ
    Hair models/Personatges/MazingerZ/cabells.obj models/Personatges/MazingerZ/images/caraMazinger.jpg
    Body models/Personatges/MazingerZ/cos.obj models/Personatges/MazingerZ/images/texture0.jpg
    Face models/Personatges/MazingerZ/Cap.obj models/Personatges/MazingerZ/images/caraMazinger.jpg
    Left_leg models/Personatges/MazingerZ/camaEsquerra.obj models/Personatges/MazingerZ/images/texture0.jpg
    Right_leg models/Personatges/MazingerZ/camaDreta.obj models/Personatges/MazingerZ/images/texture0.jpg
End

DISABLE BRIGHTNESS
#
#Entity pers-noi
#    object Hair models/Personatges/Noi/Cabells.obj models/Personatges/Noi/Imatges/hair1noCulling.JPG
#    object Body models/Personatges/Noi/Cos.obj models/Personatges/Noi/Imatges/Material__1noCulling.JPG
#    object Face models/Personatges/Noi/Cara.obj models/Personatges/Noi/Imatges/hrad1noCulling.JPG
#    object Left_leg models/Personatges/Noi/CamaEsquerra.obj models/Personatges/Noi/Imatges/Material__1noCulling.JPG
#    object Right_leg models/Personatges/Noi/CamaDreta.obj models/Personatges/Noi/Imatges/Material__1noCulling.JPG
#end
#
#Entity pers-noia
#    object Hair models/Personatges/Noia/Cabells.obj models/Personatges/Noia/Images/_01_-_Default1noCulling.JPG
#    object Body models/Personatges/Noia/Cos.obj models/Personatges/Noia/Images/_03_-_Default1noCulling.JPG
#    object Face models/Personatges/Noia/Cara.obj models/Personatges/Noia/Images/_02_-_Default1noCulling.JPG
#    object Left_leg models/Personatges/Noia/CamaEsquerra.obj models/Personatges/Noia/Images/_03_-_Default1noCulling.JPG
#    object Right_leg models/Personatges/Noia/CamaDreta.obj models/Personatges/Noia/Images/_03_-_Default1noCulling.JPG
#end

#PARETS I ESTRUCTURA
Entity Parets
        Object Paret1 models/parets/paret1.obj models/parets/TextParet2.jpg
        Object Paret2 models/parets/paret2.obj models/parets/TextParet2.jpg
        Object Paret3 models/parets/paret3.obj models/parets/TextParet2.jpg
        Object Paret4 models/parets/paret4.obj models/parets/TextParet2.jpg
        Object Paret5 models/parets/paret5.obj models/parets/TextParet2.jpg
        Object Paret6 models/parets/paret6.obj models/parets/TextParet2.jpg
        Object Paret7 models/parets/paret7.obj models/parets/TextParet2.jpg
        Object Paret8 models/parets/paret8.obj models/parets/TextParet2.jpg
End

Entity Estructura
    Object ParetCentral models/Estructura/ParetCentral.obj models/Estructura/TextParet
    Object ParetExterior models/Estructura/paretExterior.obj models/Estructura/TextParet
    Object Terra models/Estructura/TerraMuseu.obj models/Estructura/texturaTerra.jpg
    #Object Escala models/Estructura/Escala.obj
    DISABLE COLLISIONS
    Object Sostre models/Estructura/Sostre.obj models/Estructura/texturaSostre.jpg models/Estructura/bumpSostre.jpg
    ENABLE COLLISIONS
End

#LLUMS

#Light és un tipus d'entitat especial
#necessita que es donin abans dels objectes els paràmetres de llum difuse, ambient, specular i l'objecte especial spotlight (en qualsevol ordre)
#es calcularà el vector Position a partir del centre de l'objecte spotlight

#DECORACIÓ

Entity bancs
    Object banc2 models/decoracio/bancs/banc2.obj models/decoracio/bancs/Imatges/texture0.jpg
    Object banc4 models/decoracio/bancs/banc4.obj models/decoracio/bancs/Imatges/texture0.jpg
    Object banc6 models/decoracio/bancs/banc6.obj models/decoracio/bancs/Imatges/texture0.jpg
    Object banc22 models/decoracio/bancs/banc22.obj models/decoracio/bancs/Imatges/texture0.jpg
    Object banc31 models/decoracio/bancs/banc31.obj models/decoracio/bancs/Imatges/texture0.jpg
    Object banc32 models/decoracio/bancs/banc32.obj models/decoracio/bancs/Imatges/texture0.jpg
    Object banc52 models/decoracio/bancs/banc52.obj models/decoracio/bancs/Imatges/texture0.jpg
    Object banc53 models/decoracio/bancs/banc53.obj models/decoracio/bancs/Imatges/texture0.jpg
End

Entity sillons
    Object sillo11 models/decoracio/sillons/sillo11.obj models/decoracio/sillons/texture1.png
    Object sillo12 models/decoracio/sillons/sillo12.obj models/decoracio/sillons/texture1.png
    Object sillo13 models/decoracio/sillons/sillo13.obj models/decoracio/sillons/texture1.png
    Object sillo14 models/decoracio/sillons/sillo14.obj models/decoracio/sillons/texture1.png
    Object sillo15 models/decoracio/sillons/sillo15.obj models/decoracio/sillons/texture1.png
    Object sillo16 models/decoracio/sillons/sillo16.obj models/decoracio/sillons/texture1.png
    Object sillo17 models/decoracio/sillons/sillo17.obj models/decoracio/sillons/texture1.png
    Object sillo18 models/decoracio/sillons/sillo18.obj models/decoracio/sillons/texture1.png
    Object sillo31 models/decoracio/sillons/sillo31.obj models/decoracio/sillons/texture1.png
    Object sillo32 models/decoracio/sillons/sillo32.obj models/decoracio/sillons/texture1.png
    Object sillo33 models/decoracio/sillons/sillo33.obj models/decoracio/sillons/texture1.png
    Object sillo34 models/decoracio/sillons/sillo34.obj models/decoracio/sillons/texture1.png
    Object sillo35 models/decoracio/sillons/sillo35.obj models/decoracio/sillons/texture1.png
    Object sillo36 models/decoracio/sillons/sillo36.obj models/decoracio/sillons/texture1.png
    Object sillo37 models/decoracio/sillons/sillo37.obj models/decoracio/sillons/texture1.png
    Object sillo38 models/decoracio/sillons/sillo38.obj models/decoracio/sillons/texture1.png
    Object sillo38 models/decoracio/sillons/sillo61.obj models/decoracio/sillons/texture1.png
    Object sillo38 models/decoracio/sillons/sillo62.obj models/decoracio/sillons/texture1.png
End

Entity sofas
    object fustasofa11 models/decoracio/sofas/fustasofa11.obj models/decoracio/sofas/texture0
    object sofa11 models/decoracio/sofas/sofa11.obj models/decoracio/sofas/texture1.png
    object fustasofa12 models/decoracio/sofas/fustasofa12.obj models/decoracio/sofas/texture0
    object sofa12 models/decoracio/sofas/sofa12.obj models/decoracio/sofas/texture1.png
    object fustasofa13 models/decoracio/sofas/fustasofa13.obj models/decoracio/sofas/texture0
    object sofa13 models/decoracio/sofas/sofa13.obj models/decoracio/sofas/texture1.png
    object fustasofa14 models/decoracio/sofas/fustasofa14.obj models/decoracio/sofas/texture0
    object sofa14 models/decoracio/sofas/sofa14.obj models/decoracio/sofas/texture1.png
end

Entity tauletes
    object tauleta11 models/decoracio/tauletes/tauleta11.obj models/decoracio/tauletes/texture1.jpg
    object tauleta12 models/decoracio/tauletes/tauleta12.obj models/decoracio/tauletes/texture1.jpg
    object tauleta31 models/decoracio/tauletes/tauleta31.obj models/decoracio/tauletes/texture1.jpg
    object tauleta32 models/decoracio/tauletes/tauleta32.obj models/decoracio/tauletes/texture1.jpg
end

#QUADRES

#Picture ha de tenir dos objectes, painting i frame, i una imatge d'informació opcional
Entity Quadre7
    Object quadre7 models/quadres/Quadre7.obj models/quadres/Imatges/Quadre7.png
End

Picture Quadre21
    Information infoQuadres/infoQ21.png
    Painting models/quadres/Quadre21.obj models/quadres/Imatges/Quadre21.png
    Frame models/quadres/Marc21.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre22
    Information infoQuadres/infoQ22.png
    Painting models/quadres/Quadre22.obj models/quadres/Imatges/Quadre22.jpg
    Frame models/quadres/Marc22.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre23
    Information infoQuadres/infoQ23.png
    Painting models/quadres/Quadre23.obj models/quadres/Imatges/Quadre23.png
    Frame models/quadres/Marc23.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre24
    Information infoQuadres/infoQ24.png
    Painting models/quadres/Quadre24.obj models/quadres/Imatges/Quadre24.png
    Frame models/quadres/Marc24.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre31
    Information infoQuadres/infoQ31.png
    Painting models/quadres/Quadre31.obj models/quadres/Imatges/Quadre31.png
    Frame models/quadres/Marc31.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre32
    Information infoQuadres/infoQ32.png
    Painting models/quadres/Quadre32.obj models/quadres/Imatges/Quadre32.png
    Frame models/quadres/Marc32.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre33
    Information infoQuadres/infoQ33.png
    Painting models/quadres/Quadre33.obj models/quadres/Imatges/Quadre33.png
    Frame models/quadres/Marc33.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre41
    Information infoQuadres/infoQ41.png
    Painting models/quadres/Quadre41.obj models/quadres/Imatges/Quadre41.png
    Frame models/quadres/Marc41.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre42
    Information infoQuadres/infoQ42.png
    Painting models/quadres/Quadre42.obj models/quadres/Imatges/Quadre42.png
    Frame models/quadres/Marc42.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre43
    Information infoQuadres/infoQ43.png
    Painting models/quadres/Quadre43.obj models/quadres/Imatges/Quadre43.png
    Frame models/quadres/Marc43.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre44
    Information models/quadres/Imatges/Quadre44.png
    Painting models/quadres/Quadre44.obj models/quadres/Imatges/Quadre44.png
    Frame models/quadres/Marc44.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre51
    Information infoQuadres/infoQ51.png
    Painting models/quadres/Quadre51.obj models/quadres/Imatges/Quadre51.png
    Frame models/quadres/Marc51.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre52
    Information infoQuadres/infoQ52.png
    Painting models/quadres/Quadre52.obj models/quadres/Imatges/Quadre52.png
    Frame models/quadres/Marc52.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre53
    Information infoQuadres/infoQ53.png
    Painting models/quadres/Quadre53.obj models/quadres/Imatges/Quadre53.png
    Frame models/quadres/Marc53.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre54
    Information infoQuadres/infoQ54.png
    Painting models/quadres/Quadre54.obj models/quadres/Imatges/Quadre54.jpg
    Frame models/quadres/Marc54.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre55
    Information infoQuadres/infoQ55.png
    Painting models/quadres/Quadre55.obj models/quadres/Imatges/Quadre55.png
    Frame models/quadres/Marc55.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre61
    Information infoQuadres/infoQ61.png
    Painting models/quadres/Quadre61.obj models/quadres/Imatges/Quadre61.png
    Frame models/quadres/Marc61.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre62
    Information infoQuadres/infoQ62.png
    Painting models/quadres/Quadre62.obj models/quadres/Imatges/Quadre62.png
    Frame models/quadres/Marc62.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre63
    Information infoQuadres/infoQ63.png
    Painting models/quadres/Quadre63.obj models/quadres/Imatges/Quadre63.png
    Frame models/quadres/Marc63.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre64
    Information infoQuadres/infoQ64.png
    Painting models/quadres/Quadre64.obj models/quadres/Imatges/Quadre64.png
    Frame models/quadres/Marc64.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre65
    Information infoQuadres/infoQ65.png
    Painting models/quadres/Quadre65.obj models/quadres/Imatges/Quadre65.png
    Frame models/quadres/Marc65.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre66
    Information infoQuadres/infoQ66.png
    Painting models/quadres/Quadre66.obj models/quadres/Imatges/Quadre66.png
    Frame models/quadres/Marc66.obj models/quadres/Imatges/texture0.jpg
End

Picture Quadre67
    Information infoQuadres/infoQ67.png
    Painting models/quadres/Quadre67.obj models/quadres/Imatges/Quadre67.png
    Frame models/quadres/Marc67.obj models/quadres/Imatges/texture0.jpg
End

Entity dona
    object dona models/decoracio/estatues/dona.obj models/decoracio/estatues/Imatges/texture1.jpg
    object pilar models/decoracio/estatues/pilar.obj models/decoracio/estatues/Imatges/texture0.jpg
End

entity flors
    object flor models/decoracio/estatues/flor.obj models/decoracio/estatues/Imatges/texFlors.jpg
    object flor2 models/decoracio/estatues/flor2.obj models/decoracio/estatues/Imatges/texFlors.jpg
    object flor3 models/decoracio/estatues/flor3.obj models/decoracio/estatues/Imatges/texFlors.jpg
    object flor4 models/decoracio/estatues/flor4.obj models/decoracio/estatues/Imatges/texFlors.jpg
End

ENABLE BRIGHTNESS

Entity font
    Object aigua models/decoracio/font/aigua.obj models/decoracio/font/texture2.jpg
    Object grabats models/decoracio/font/grabats.obj models/decoracio/font/texture0.jpg
    Object marbre models/decoracio/font/marbre.obj models/decoracio/font/texture1.jpg
end

Entity qu63
    object prova models/quadres/Quadre63.obj models/quadres/Imatges/Quadre63.png
end

Entity cameres
    object camera1 models/decoracio/cameres/camera1.obj models/decoracio/cameres/texture0.png
    object camera4 models/decoracio/cameres/camera4.obj models/decoracio/cameres/texture0.png
    object camera7 models/decoracio/cameres/camera7.obj models/decoracio/cameres/texture0.png
    object camera21 models/decoracio/cameres/camera21.obj models/decoracio/cameres/texture0.png
    object camera22 models/decoracio/cameres/camera22.obj models/decoracio/cameres/texture0.png
    object camera31 models/decoracio/cameres/camera31.obj models/decoracio/cameres/texture0.png
    object camera32 models/decoracio/cameres/camera32.obj models/decoracio/cameres/texture0.png
    object camera51 models/decoracio/cameres/camera51.obj models/decoracio/cameres/texture0.png
    object camera61 models/decoracio/cameres/camera61.obj models/decoracio/cameres/texture0.png
    object camera62 models/decoracio/cameres/camera62.obj models/decoracio/cameres/texture0.png
end

Entity David
    object david models/decoracio/estatues/david.obj models/decoracio/estatues/Imatges/blanc.jpg
End

Entity pegasus
    object pegasus models/decoracio/estatues/pegasus.obj models/decoracio/estatues/Imatges/daurat.jpg
End
#
#Entity esculturaGuai
#    object esculGuai models/decoracio/estatues/estatuaguai.obj models/decoracio/estatues/Imatges/blanc.jpg
#End

Entity Light llum0
        Difuse 0.2 0.2 0.2 1.0
        Ambient 0.03 0.03 0.03 1.0
        Specular 0.2 0.2 0.2 1.0
        Spotlight models/llums/llum0.obj models/llums/Imatges/llum.jpg
        Object lampada models/llums/lampada0.obj models/llums/Imatges/texturaMetall.jpg
End
Entity Light llum1
        Difuse 0.2 0.2 0.2 1.0
        Ambient 0.03 0.03 0.03 1.0
        Specular 0.2 0.2 0.2 1.0
        Spotlight models/llums/llum1.obj models/llums/Imatges/llum.jpg
        Object lampada models/llums/lampada1.obj models/llums/Imatges/texturaMetall.jpg
End
Entity Light llum2
        Difuse 0.2 0.2 0.2 1.0
        Ambient 0.03 0.03 0.03 1.0
        Specular 0.2 0.2 0.2 1.0
        Spotlight models/llums/llum2.obj models/llums/Imatges/llum.jpg
        Object lampada models/llums/lampada2.obj models/llums/Imatges/texturaMetall.jpg
End
Entity Light llum3
        Difuse 0.2 0.2 0.2 1.0
        Ambient 0.03 0.03 0.03 1.0
        Specular 0.2 0.2 0.2 1.0
        Spotlight models/llums/llum3.obj models/llums/Imatges/llum.jpg
        Object lampada models/llums/lampada3.obj models/llums/Imatges/texturaMetall.jpg
End
Entity Light llum4
        Difuse 0.15 0.15 0.15 1.0
        Ambient 0.03 0.03 0.03 1.0
        Specular 0.15 0.15 0.15 1.0
        Spotlight models/llums/llum4.obj models/llums/Imatges/llum.jpg
        Object lampada models/llums/lampada4.obj models/llums/Imatges/texturaMetall.jpg
End
Entity Light llum5
        Difuse 0.15 0.15 0.15 1.0
        Ambient 0.03 0.03 0.03 1.0
        Specular 0.15 0.15 0.15 1.0
        Spotlight models/llums/llum5.obj models/llums/Imatges/llum.jpg
        Object lampada models/llums/lampada5.obj models/llums/Imatges/texturaMetall.jpg
End
