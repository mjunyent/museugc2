#ifndef SHADER_H
#define SHADER_H
#include <QGLShaderProgram>

class Shader
{
public:
    Shader(bool readyOpenGL2);
    ~Shader();
    void eliminaShaders();
    bool inicialitzaShaders();
    bool activaPhong();
    bool activaGouraud();
    bool activaSimple();
    bool activaBumpMapping();
    bool actiu;
    QGLShaderProgram *phong;
    QGLShaderProgram *gouraud;
    QGLShaderProgram *simple;
    QGLShaderProgram *bump;
    int tangentLoc;
    int bitangentLoc;
private:
    bool initializeShaders(QGLShaderProgram *sha, QString filename);
    bool openGL2Ready;
};

#endif // SHADER_H
