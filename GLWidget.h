#ifndef _GLWIDGET_H
#define _GLWIDGET_H

#include "Util.h"
#include <QtOpenGL/QGLWidget>
#include <assimp/include/assimp/Importer.hpp>
#include <vector>
#include <string>
#include <QtGui/QMouseEvent>
#include <QApplication>
#include <QTimer>
#include "Personatge.h"
#include "Camera.h"
#include "Client.h"
#include "Mapa.h"
#include "Objecte.h"
#include "Entitat.h"
#include "Quadre.h"
#include "Persona.h"
#include "Llum.h"
#include "Frustum.h"
#include "Shader.h"
#include <QElapsedTimer>

#define DISTANCIA_INFO_QUADRE 15
#define FPS_MAX 60

#define MAXIM_A_PROP 1
#define MAXIM_LLUNY 5
#define ZOOM_WHEEL 0.25

#define DESACTIVAT 0
#define GOURAUD 1
#define PHONG 2


class GLWidget : public QGLWidget {

    Q_OBJECT // must include this if you use Qt signals/slots

public:
    GLWidget(QWidget *parent = NULL);
    ~GLWidget();   
    bool controlTeclatIRatoli;
    Client *client;
    std::vector<Persona> Persones;
    bool mostrantMenu;
    void uniformsShaders(QGLShaderProgram* shader);
    void activarControl();
    float sight3rd;
    bool openGL2OK;
    bool shadersON;
    Personatge *pers;

signals:
    void mostraMenu();
    void continua();
    void escriureXat();
    void prova();
    void amagaXat();
    void mostraXat();
    void canviaVisualitzacio();

public slots:
    void processaTimer();

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    Shader *shader;
    Camera *camera;
    Mapa *mapa;
    QTimer *timerActualitzacio; //cada quan cridem updateGL()
    QElapsedTimer *timerGeneral;
    Assimp::Importer *importador;
    std::vector<Entitat> Entitats;
    std::vector<Quadre> Quadres;
    Frustum* frustum;
    bool modPers;
    float w;
    float h;
    Point2D posCam;
    Quadre* quadreInfo;

    bool loadMuseu(std::string fileName);
    bool llegirVector(char* buffer, float* vector, int dim);
    bool getPropietat(bool enable, char* buffer, bool* colisions, bool* brilla);
    bool llegirObjecte(Objecte* obj, char* buffer, bool colisiona, bool brilla);
    void mostraMinimapa();
};

#endif  /* _GLWIDGET_H */
