#ifndef PERSONATGE_H
#define PERSONATGE_H

#define USERNAME_LENGTH 20 //també s'ha de canviar al common.h
//CONSTANTS
#define PI 3.14159265
#define VELOCITAT 5.0 //unitats per segon
#define VELOCITAT_ROTACIO 40 //graus per segon
#define VELOCITAT_DESPLASAMENT 10 //desplaçament del personatge, ha de ser superior a 1

#include "Persona.h"

class Personatge
{
public:
    Personatge();
    void mouEsquerra();
    void mouDreta();
    void mouEndavant();
    void mouEnrere();
    void calculaPassaFutura(GLfloat *x, GLfloat *z);
    void update(double timeElapsed);
    void setPersona(Persona *persona);
    void dibuixa();
    char nom[USERNAME_LENGTH];
    int id;
    float x;
    float y;
    float z;
    float prex,prez;
    float angleE,angleD;
    bool primerCop;
    bool primerCop1;
    float increment;
    QElapsedTimer timer;
    QElapsedTimer timer2;
    float orientacioh;
    float orientaciov;
    float despx;
    float despz;
    float roth;
    float rotv;
    bool tePersona;
    Persona* persona;
};

#endif // PERSONATGE_H
