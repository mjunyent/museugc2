#ifndef LLUM_H
#define LLUM_H

#include "Entitat.h"

#define MAX_LLUMS 8

class Llum : public Entitat
{
public:
    Llum(float pos[4], float dif[4], float amb[4], float spe[4], int index);
private:
    float pos[4],dif[4],amb[4],spe[4];
    int index;
};

#endif // LLUM_H
