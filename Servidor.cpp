#include "Common.h"
#include <sys/time.h>		//pel select()
#include <sys/types.h>		//pel select()
#include <vector>
#include <string>
using namespace std; //per poder posar vector directament, en comptes de std::vector

#define MAX_PENDING 5    	/* Maximum outstanding connection requests */
#define SEC 0				//	|	temps que estarem esperant per novetats,
#define NANOSEC 50000000	//	|	passat aquest temps mirarem si hem rebut alguna cosa des del client
#define MAX_POSICIONS 50 	//ens guardarem les ultimes MAX_POSICIONS posicions
#define MAX_MISSATGES 10 	//ens guardarem els ultims MAX_MISSATGES missatges
#define MAX_LOGOUTS 10		//ens guardarem els ultims MAX_LOGOUTS ids dels clients que han fet logout
#define MAX_NUM_NOVETATS 1000 //numero a partir del qual tornara a 0 la id que assignem a cada novetat

//VARIABLES GLOBALS (amb els seus respectius mutex)
	//novetats:
pthread_cond_t novetats = PTHREAD_COND_INITIALIZER; //creem la variable condicional que ens avisarà quan hi hagi novetats (tant missatges com posicions)

pthread_mutex_t mutexEntradaActual = PTHREAD_MUTEX_INITIALIZER;
int entradaActual = -1;

pthread_mutex_t mutexPosicions = PTHREAD_MUTEX_INITIALIZER;
user posicions[MAX_POSICIONS];
int indexPosicioActual = -1;

pthread_mutex_t mutexLogouts = PTHREAD_MUTEX_INITIALIZER;
int logouts[MAX_LOGOUTS];
int indexLogoutActual = -1;

pthread_mutex_t mutexMissatges = PTHREAD_MUTEX_INITIALIZER;
message missatges[MAX_MISSATGES];
int indexMissatgeActual = -1; //protegit per MutexIndexNovetats

	//usuaris:
pthread_mutex_t mutexUsersInfo = PTHREAD_MUTEX_INITIALIZER;
int nextID = 0; //ID que donarem al següent client que fagi log in
vector<user> userVector(0); //vector on hi guardarem la informació dels usuaris


//FUNCIONS I PROCEDIMENTS
bool rebre(int sock, char* bufferRebre, int length);
bool sendDeny(int sock);
bool logout(int sock, int id, bool connectat);
void *handle(void *sock_client);
void registerMessage(int fromID, char* m);
void registerLocation(user *loc, int id);
int registerNewUser(int sock, user usuari, int* ultimaEntrada, int* ultimMissatge, int* ultimaPosicio, int* ultimLogout, int *id);
bool actualitzarMissatges(int sock, int id, int *ultimMissatge, int *ultimaEntrada);
bool actualitzarPosicions(int sock, int id, int *ultimaPosicio, int *ultimaEntrada);
bool actualitzarLogouts(int sock, int id, int *ultimLogout, int *ultimaEntrada);

//funcio que retorna true si rebem la quantitat de bytes esperada i que posa el que hem rebut al buffer que passem per referencia
bool rebre(int sock, char* bufferRebre, int length){
	int recvMsgSize;

	//si retorna 0 vol dir que ens han tallat la connexió correctament amb el handshake, -1 voldrà dir que TCP ha rebut un RESET
	recvMsgSize = recv(sock, bufferRebre, length, 0);
	return (recvMsgSize == length); 
}
bool sendDeny(int sock){
	char enviarDeny = DENY;
	int length;
	
	length = sizeof(char);
	if (send(sock, &enviarDeny, length, 0) != length)
		return false;
	return true;
}

void registerMessage(int fromID, message *missatge){
	pthread_mutex_lock(&mutexMissatges); 	//agafem el mutex per modificar la llista de missatges
	//incrementem l'index
	indexMissatgeActual = (indexMissatgeActual + 1) % MAX_MISSATGES;
	//posem els valors a l'estructura
	memcpy(&missatges[indexMissatgeActual], missatge, sizeof(message));
	missatges[indexMissatgeActual].fromID = fromID; //no ens fiem del client i hi posem nosaltres la seva id
	
	//Agafem un número d'entrada (com si fos un tiquet, és un identificador)
	pthread_mutex_lock(&mutexEntradaActual);
	entradaActual = (entradaActual + 1) % MAX_NUM_NOVETATS;
	pthread_mutex_unlock(&mutexEntradaActual);
	
	printf("Missatge: %s\n", missatge->content);
	pthread_mutex_unlock(&mutexMissatges);

	//despertem als threads que puguin estar dormint
	pthread_cond_broadcast(&novetats);
}

void registerLocation(user *loc, int id){
	
	//Llista d'usuaris
	pthread_mutex_lock(&mutexUsersInfo);
	//actualitzem la informació de l'usuari
	for(vector<user>::iterator u = userVector.begin(); u != userVector.end(); ++u){
		if(u->id == id){
			u->x = loc->x;
			u->y = loc->y;
			u->z = loc->z;
			u->orientation = loc->orientation;
			break;
		}
	}
	pthread_mutex_unlock(&mutexUsersInfo);
	
	pthread_mutex_lock(&mutexPosicions);
	indexPosicioActual = (indexPosicioActual + 1) % MAX_POSICIONS;
	//posem els valors a l'estructura
	memcpy(&posicions[indexPosicioActual], loc, sizeof(user));
	posicions[indexPosicioActual].id = id;
	
	//Agafem un número d'entrada (com si fos un tiquet, és un identificador)
	pthread_mutex_lock(&mutexEntradaActual);
	entradaActual = (entradaActual + 1) % MAX_NUM_NOVETATS;
	pthread_mutex_unlock(&mutexEntradaActual);
	
	pthread_mutex_unlock(&mutexPosicions);
	
	//despertem als threads que puguin estar dormint
	pthread_cond_broadcast(&novetats);
}
int registerNewUser(int sock, user usuari, int* ultimaEntrada, int* ultimMissatge, int* ultimaPosicio, int* ultimLogout, int *ClientID){
	int id;
	int length;
	bool userNameExisteix = false;

	//estructures de dades per enviar al client
	char bufferLoginOK[sizeof(char)+sizeof(int)]; //opcode+id
	char bufferEnviarUser[sizeof(char)+sizeof(user)]; //opcode + user

	//agafem el mutex del vector d'usuaris
	pthread_mutex_lock(&mutexUsersInfo);
	//comprovar que el nom d'usuari no s'esta utilitzant
	for(vector<user>::iterator u = userVector.begin(); u != userVector.end(); ++u){
		if(strcmp(u->username, usuari.username) == 0){
			userNameExisteix = true;
			break;
		}
	}
	if (userNameExisteix){ //si el nom d'usuari ja existeix, enviem l'opcode deny
		pthread_mutex_unlock(&mutexUsersInfo); //alliberem el mutex
		id = -1;
		return sendDeny(sock);
	}else{
		//creem l'usuari
		id = nextID;
		usuari.id = id;
		nextID++; //també esta protegit per mutexUserInfo
		//guardem l'usuari
		userVector.push_back(usuari);
		
		//alliberem el mutex durant una estona
		pthread_mutex_unlock(&mutexUsersInfo);
		
		//Enviem l'acceptació del login al client
		//posem l'opcode i la id al buffer
		bufferLoginOK[0] = ACCEPT;
		memcpy(bufferLoginOK+1, &id, sizeof(int));
		length = sizeof(bufferLoginOK);
		
		if (send(sock, bufferLoginOK, length, 0) != length){
			return false;
		}	
			
		//enviem la informació de tots els usuaris que estàn connectats (excepte la informació del client, que serà l'últim del vector)
		length = sizeof(bufferEnviarUser);
		bufferEnviarUser[0] = USER;

		pthread_mutex_lock(&mutexUsersInfo);
		for(std::vector<user>::size_type i = 0; i < userVector.size()-1; i++){
			memcpy(bufferEnviarUser+1, &userVector[i], sizeof(user));
			if (send(sock, bufferEnviarUser, length, 0) != length){
				return false;
			}
		}
		pthread_mutex_unlock(&mutexUsersInfo);
	
		pthread_mutex_lock(&mutexPosicions);
		indexPosicioActual = (indexPosicioActual + 1) % MAX_POSICIONS;
		//posem el nou client com a novetat pels altres threads
		memcpy(&posicions[indexPosicioActual], &usuari, sizeof(user));
		posicions[indexPosicioActual].id = id;
		
		//Agafem un número d'entrada (com si fos un tiquet, és un identificador)
		pthread_mutex_lock(&mutexEntradaActual);
		entradaActual = (entradaActual + 1) % MAX_NUM_NOVETATS;
		//li donem l'ultima entrada al client perquè comenci a escoltar les novetats a partir d'aquí
		*ultimaEntrada = entradaActual;
		pthread_mutex_unlock(&mutexEntradaActual);
		
		//li donem l'index de posicions al client
		*ultimaPosicio = indexPosicioActual;
		pthread_mutex_unlock(&mutexPosicions);
			
		//despertem als threads que puguin estar dormint perque hi ha novetats: un nou usuari!
		pthread_cond_broadcast(&novetats);
		
		pthread_mutex_lock(&mutexMissatges);
		//li donem l'index de missatges al client perque comenci a escoltar novetats de missatges a partir d'aqui
		*ultimMissatge = indexMissatgeActual;
		pthread_mutex_unlock(&mutexMissatges);
		
		pthread_mutex_lock(&mutexLogouts);
		*ultimLogout = indexLogoutActual;
		pthread_mutex_unlock(&mutexLogouts);
				
		printf("New user: %s\n", usuari.username);
		*ClientID = id;
		return true;
	}
}

bool actualitzarMissatges(int sock, int id, int *ultimMissatge, int *ultimaEntrada){
	char bufferEnviar[sizeof(char)+sizeof(message)];
	int i = *ultimMissatge;
	int comptador = *ultimaEntrada;
	int length;
	
	pthread_mutex_lock(&mutexMissatges);
	while(i != indexMissatgeActual){
		i = (i + 1) % MAX_MISSATGES;
		//mirem si el missatge és per nosaltres o és per tothom, sino l'ignorem
		//si ve de nosaltres, també l'ignorem
		if ((missatges[i].toID == id || missatges[i].toID == -1) && !missatges[i].fromID == id){
			bufferEnviar[0] = MESSAGE;
			memcpy(bufferEnviar+1, &missatges[i], sizeof(message));
			length = sizeof(bufferEnviar);
			if (send(sock, bufferEnviar, length, 0) != length)
				return false;
			comptador = (comptador + 1) % MAX_NUM_NOVETATS; //compta la quantitat de missatges que llegim
		}
	}
	*ultimMissatge = indexMissatgeActual;
	*ultimaEntrada = comptador;
	pthread_mutex_unlock(&mutexMissatges);
	return true;
}

bool actualitzarLogouts(int sock, int id, int *ultimLogout, int *ultimaEntrada){
	char bufferEnviar[sizeof(char)+sizeof(int)];
	int i = *ultimLogout;
	int comptador = *ultimaEntrada;
	int length;
	
	pthread_mutex_lock(&mutexLogouts);
	while(i != indexLogoutActual){
		i = (i + 1) % MAX_LOGOUTS;
		bufferEnviar[0] = LOGOUT;
		memcpy(bufferEnviar+1, &logouts[i], sizeof(int));
		length = sizeof(bufferEnviar);
		if (send(sock, bufferEnviar, length, 0) != length)
			return false;
		comptador = (comptador + 1) % MAX_NUM_NOVETATS;
	}
	*ultimLogout = indexLogoutActual;
	*ultimaEntrada = comptador;
	pthread_mutex_unlock(&mutexLogouts);
	return true;
}

bool actualitzarPosicions(int sock, int id, int *ultimaPosicio, int *ultimaEntrada){
	char bufferEnviar[sizeof(char)+sizeof(user)];
	int i = *ultimaPosicio;
	int comptador = *ultimaEntrada;
	int length;
	
	pthread_mutex_lock(&mutexPosicions);
	while(i != indexPosicioActual){
		i = (i + 1) % MAX_POSICIONS;
		bufferEnviar[0] = USER;
		memcpy(bufferEnviar+1, &posicions[i], sizeof(user));
		length = sizeof(bufferEnviar);
		if (send(sock, bufferEnviar, length, 0) != length)
			return false;
		comptador = (comptador + 1) % MAX_NUM_NOVETATS; //compta la quantitat de posicions que llegim
	}
	*ultimaPosicio = indexPosicioActual;
	*ultimaEntrada = comptador;
	pthread_mutex_unlock(&mutexPosicions);
	return true;
}

bool logout(int sock, int id, bool connectat){
	char opcode = ACCEPT;
	int length;
	
	pthread_mutex_lock(&mutexUsersInfo);
	for(vector<user>::iterator u = userVector.begin(); u != userVector.end(); ++u){
		if(u->id == id){
			printf("User %s has logged out.\n", u->username);
			userVector.erase(u);
			break;
		}
	}
	pthread_mutex_unlock(&mutexUsersInfo);

	pthread_mutex_lock(&mutexLogouts); 	//agafem el mutex per modificar la llista
	//incrementem l'index
	indexLogoutActual = (indexLogoutActual + 1) % MAX_LOGOUTS;
	logouts[indexLogoutActual] = id;
	
	//Agafem un número d'entrada (com si fos un tiquet, és un identificador)
	pthread_mutex_lock(&mutexEntradaActual);
	entradaActual = (entradaActual + 1) % MAX_NUM_NOVETATS;
	pthread_mutex_unlock(&mutexEntradaActual);
	
	pthread_mutex_unlock(&mutexLogouts);
	pthread_cond_broadcast(&novetats);
	
	length = sizeof(char);
	if (connectat){
		if (send(sock, &opcode, length, 0) != length)
			return false;
	}
	return true;
}

//TODO: utilitzar hton i ntoh per intercanviar numeros amb el client
void *handle(void *sock_client)
{
	int sock = *(int *)sock_client; //convertim de punter a void a enter
    char bufferRebre[BUFFER_SIZE];
    bool loggedIn = false;
	int clientID;
	char opcode;
	int res;
	bool connectat = true;
	int ultimaEntrada; //ultima novetat (tant missatge com posicio) que hem tractat
	int ultimMissatge; //ultim missatge que hem enviat al client
	int ultimaPosicio; //ultima posicio que hem enviat al client
	int ultimLogout;
	struct timespec timeoutNovetats; 	// |	Aquí posarem el temps de timeout
	struct timeval timeoutClient; 		// |
	fd_set fds; // conjunt de bits que ens marquen a quins file descriptors fer cas
        
/*
 * FUNCIONAMENT - Cada thread fa el següent:
 * 
 * 		1. Mirar si hi ha novetats
 * 			- Sí: Passem al pas 2.
 * 			- No: Bloquejar-se per un temps determinat o fins que hi hagi novetats i anar al pas 2.
 * 
 * 		2. Hi ha novetats?
 * 			- Sí -> 
 * 					· Mirem si hi ha missatges.
 * 					· Mirem si hi ha posicions.
 * 					· Passem al pas 3.
 * 			- No -> Passem al pas 3.
 * 
 * 		3. Mirar si el client ens ha enviat alguna cosa
 * 			- Sí -> fem el que calgui i tornem al pas 1.
 * 			- No -> passem al pas 1.
 * */
 
	//inicialitzem el conjunt a 0 per a la funcio select()
	FD_ZERO(&fds); // Macro que ens omplira fds amb zeros

	//posem el timeout a 0 perquè no s'esperi, simplement si s'ha rebut alguna cosa del client, contestarem, sino tornem a estar pendents de novetats
	timeoutClient.tv_sec = 0;
	timeoutClient.tv_usec = 0;
	
	do{		
		if(!loggedIn){
			if((connectat = rebre(sock, bufferRebre, sizeof(char)))){
				if(*bufferRebre == LOGIN){
					//rebem la peticio de login
					if((connectat = rebre(sock, bufferRebre, sizeof(user)))){
						if ((connectat = registerNewUser(sock, *((user *)(bufferRebre)), &ultimaEntrada, &ultimMissatge, &ultimaPosicio, &ultimLogout, &clientID)) != -1){
							loggedIn = true;
						}
					}
				} else {
					sendDeny(sock);
					connectat = false;
				}
			}
		} else {
			//PAS 1
			//s'ha de tornar a posar el valor de segons i nanosegons que volem a l'estructura, ja que es va modificant
			timeoutNovetats.tv_sec = time(NULL) + SEC;
			timeoutNovetats.tv_nsec = NANOSEC;
			pthread_mutex_lock(&mutexEntradaActual);
			if (entradaActual == ultimaEntrada){ //no hi ha novetats -> ens posem a dormir
				pthread_cond_timedwait(&novetats, &mutexEntradaActual, &timeoutNovetats); //això ja allibera el mutex i posa el thread a dormir. Es despertarà o bé quan hi hagi novetats o quan havi passat X temps.
			}
			//PAS 2
			//al tornar de dormir tornem a tenir el mutex :)
			//tornem a mirar si hi ha novetats
			if (entradaActual == ultimaEntrada){
				//segueix sense haver-hi novetats, ens hem despertat pel timeout, anem directe a veure si hem rebut alguna cosa del client
				pthread_mutex_unlock(&mutexEntradaActual);
			} else {
				pthread_mutex_unlock(&mutexEntradaActual);
				//hi ha novetats! actualitzem els missatges i les posicions
				connectat = actualitzarMissatges(sock, clientID, &ultimMissatge, &ultimaEntrada);
				connectat = actualitzarPosicions(sock, clientID, &ultimaPosicio, &ultimaEntrada);
				connectat = actualitzarLogouts(sock, clientID, &ultimLogout, &ultimaEntrada);
			}
				
			//PAS 3
			//Mirem si hem rebut alguna cosa del client
			//Quan select retorna 0 (que vol dir que no hem rebut res per cap dels descriptors que li passem al conjunt) treu el file descriptor del conjunt
			//per tant cada vegada l'hem d'afegir.
			FD_SET(sock, &fds); // Posem el nostre socket al conjunt
			while (connectat && loggedIn && (res = select(sock+1, &fds, NULL, NULL, &timeoutClient)) > 0){
				//Hem rebut alguna cosa del client! Agafem l'opcode
				if((connectat = rebre(sock, bufferRebre, sizeof(char)))){
					//si no es tracta de cap error ni de fi de connexio, mirem què ens envia el client
					opcode = *bufferRebre;
					switch(opcode){
						case LOGOUT:
							connectat = logout(sock, clientID, true);
							loggedIn = false;
							break;
						case MESSAGE:
							//rebem el missatge destinat a tothom
							if((connectat = rebre(sock, bufferRebre, sizeof(message)))){
								registerMessage(clientID, (message*)bufferRebre); //toID = -1 ja que ho volem enviar a tots, sino aquí hi aniria la ID a qui enviar-li
							}
							break;
						case USER:
							//rebem la localització del client
							if((connectat = rebre(sock, bufferRebre, sizeof(user)))){
								registerLocation((user*)bufferRebre, clientID);
							}
							break;
						default:
							//rebem alguna cosa desconneguda, enviem un error i tallem la connexió
							sendDeny(sock);
							connectat = false;
							break;
					}
				}
			}
			if (res ==-1){
				//comprovem que el select no hagi donat un error
				connectat = false;
			}
		}
    } while (connectat); //mentre duri la connexió, fem el bucle

	if (loggedIn)
		logout(sock, clientID, false);
    close(sock);
    pthread_exit(0);
}

int main(int argc, char *argv[])
{
    int sock;                    		// Socket descriptor for server
    int client;                    		// Socket descriptor for client
    struct sockaddr_in serverAddr; 		// Local address
    struct sockaddr_in clientAddr; 		// Client address
    unsigned short port = DEFAULT_PORT;	// Server port
    unsigned int addrLen;            	// Length of client address data structure
	int reuseaddr = 1; 					// Valor true per activar la reutilització del port
    if (argc < 1 || argc > 2)     		// Test for correct number of arguments
    {
        fprintf(stderr, "Usage:  %s [<Server Port>]\n", argv[0]);
        exit(1);
    }
	if (argc == 2)
		port = atoi(argv[1]);  // First arg:  local port
		
	printf("Initializing...\n");

    // Create socket for incoming connections
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		return -1;

    // Construct local address structure
    memset(&serverAddr, 0, sizeof(serverAddr));   // Zero out structure
    serverAddr.sin_family = AF_INET;                // Internet address family
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY); // Any incoming interface
    serverAddr.sin_port = htons(port);      // Local port

    // Enable the socket to reuse the address - more than one socket can be connected to the same port
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(sock)) < 0) {
        return -1;
    }

    // Bind to the local address
    if (bind(sock, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
        return -1;

    // Mark the socket so it will listen for incoming connections
    if (listen(sock, MAX_PENDING) < 0)
       return -1;

	printf("Server Ready. Listening on port %d.\n", port);
    while (true)
    {
		pthread_t thread;
		
        // Set the size of the in-out parameter
        addrLen = sizeof(clientAddr);

        // Wait for a client to connect
        if ((client = accept(sock, (struct sockaddr *) &clientAddr,
                               &addrLen)) < 0)
            return -1;

        printf("Handling client %s\n", inet_ntoa(clientAddr.sin_addr));
        
        if (pthread_create(&thread, NULL, handle, &client) != 0){
                printf("Failed to create thread\n");
		}
    }
}
