#include "Personatge.h"
#include "math.h"
Personatge::Personatge(){
    x = 10.0;
    y = 1.8;
    z = 10.0;
    orientacioh = 180.0;
    orientaciov = 0.0f;
    roth=0.0;
    rotv=0.0;
    despx = 0.0;
    despz = 0.0;
    primerCop=true;
    primerCop1=true;
    tePersona = false;
}

void Personatge::mouEsquerra()
{
    float orientacio=this->orientacioh*PI/180;
    this->despx=(-cosf(orientacio));
    this->despz=(-sinf(orientacio));
}

void Personatge::mouDreta()
{
    float orientacio=this->orientacioh*PI/180;
    this->despx=-(-cosf(orientacio));
    this->despz=-(-sinf(orientacio));
}

void Personatge::mouEndavant()
{
    float orientacio=this->orientacioh*PI/180;

    this->despx=-(-sinf(orientacio));
    this->despz=-(cos(orientacio));
}

void Personatge::mouEnrere()
{
    float orientacio=this->orientacioh*PI/180;

    this->despx=(-sinf(orientacio));
    this->despz=(cos(orientacio));
}

void Personatge::update(double timeElapsed){
    if (primerCop && tePersona){
        //this->persona->Cara.calcularBoundingSphere();
        this->y=this->persona->Cara.centre[1];
    }
    this->orientacioh += this->roth*VELOCITAT_ROTACIO*timeElapsed;
    this->orientaciov += this->rotv*VELOCITAT_ROTACIO*timeElapsed;

    //orientacioh i orientaciov mod 360
    if (this->orientacioh<0)this->orientacioh+=360;
    if (this->orientacioh>360)this->orientacioh-=360;
    if (this->orientaciov<-90)this->orientaciov=-90;
    if (this->orientaciov>85)this->orientaciov=85;

    x += despx*VELOCITAT*timeElapsed;
    z += despz*VELOCITAT*timeElapsed;
    despx = 0.0;
    despz = 0.0;

}

void Personatge::calculaPassaFutura(GLfloat *x, GLfloat *z){
    *x += despx*VELOCITAT;
    *z += despz*0.03;
}

void Personatge::setPersona(Persona* persona){
    this->persona = persona;
    tePersona = true;
}

void Personatge::dibuixa(){
    if(tePersona){
        if(primerCop1){
            prex=x;
            prez=z;
            primerCop1=false;
            increment=1.0;
            timer.start();
            timer2.start();
        }
        if(x+z!=prex+prez){
            timer.restart();
        }

        if (timer.elapsed()<100){
            if (timer2.elapsed()>20){
                if(angleD==PASSA/2 || angleE==PASSA/2){
                    increment=-increment;
                }
                angleD+=increment*VELOCITAT*PASSA/PASSAM*timer2.elapsed()/1000;
                angleE-=increment*VELOCITAT*PASSA/PASSAM*timer2.elapsed()/1000;
                if(angleD>PASSA/2){
                    angleD=PASSA/2;
                    angleE=-PASSA/2;
                }
                else if (angleE>PASSA/2) {
                    angleE=PASSA/2;
                    angleD=-PASSA/2;
                }
                timer2.restart();
            }
        }
        else{
            angleD=0;
            angleE=0;
        }
        this->persona->dibuixa(x,0,z,orientacioh, angleD, angleE);
        prex=x;
        prez=z;
    }
}
