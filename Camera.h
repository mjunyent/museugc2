#ifndef CAMERA_H
#define CAMERA_H

#include "Util.h"
#include "Common.h"
#include "Personatge.h"

class Camera
{
public:
    Camera();
    ~Camera();

    void setProjection(int w, int h);
    void setOrthogonal(int w, int h, int R);
    //void setAlsat();
    void setPlanta();
    void update(Personatge *pers, float dist);

private:
    float near,far, alpha;
};

#endif // CAMERA_H
