#ifndef MUSEUWINDOW_H
#define MUSEUWINDOW_H

#include <QMainWindow>

namespace Ui {
class MuseuWindow;
}

class MuseuWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MuseuWindow(QWidget *parent = 0);
    ~MuseuWindow();

public slots:
    void mostraMenu();
    void continua();
    void mostraAboutUs();
    void sortir();
    void mostraConnectToServer();
    void connectToServer();
    void nouMissatge(QString nom, QString missatge, bool privat);
    void canviaScreenMode(int index);
    void canviaShaders(int index);
    void canviaModeJoc(int index);
    void escriureXat();
    void xatEditingFinished();
    void mostraOptions();
    void amagaXat();
    void mostraXat();
    void canviaVisualitzacio();
    void canviaPersona(int index);

private:
    Ui::MuseuWindow *ui;
    bool fullscreen;
    bool primercop;
};

#endif // MUSEUWINDOW_H
