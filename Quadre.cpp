#include "Quadre.h"
#include <QSize>

Quadre::Quadre(Objecte m, Objecte p, QImage* inf)
{
    marc = m;
    pintura = p;

    //Agafem com a normal del quadre la del primer vertex de la pintura (al ser un pla ja ens serveix, no cal fer la mitjana)
    normal[0] = pintura.cara[0].vertex[0].nx;
    normal[1] = pintura.cara[0].vertex[0].ny;
    normal[2] = pintura.cara[0].vertex[0].nz;

    //Agafem com a centre el centre de la pintura
    centre[0] = pintura.centre[0];
    centre[1] = pintura.centre[1];
    centre[2] = pintura.centre[2];

    if(inf == NULL){
        hasInfo = false;
    }else{
        hasInfo = true;
        info = *inf;
    }
}

void Quadre::dibuixa(Shader *shader, Frustum *frustum, bool simple, bool exigimColisions){
    if(frustum->sphereInFrustum(marc.centre[0], marc.centre[1], marc.centre[2], marc.radi)){
        //si el quadre és dins del frustum
        //IMPORTANT: dibuixar primer pintura i després marc!
        if(simple){
            shader->activaSimple();
            pintura.dibuixa();
            marc.dibuixa();
        }else{
            if(pintura.hasNormalTexture)
                shader->activaBumpMapping();
            else
                shader->activaGouraud();
            pintura.dibuixa(exigimColisions);
            if(marc.hasNormalTexture)
                shader->activaBumpMapping();
            else
                shader->activaGouraud();
            marc.dibuixa(exigimColisions);
        }
    }
}

void Quadre::showInfo(int w, int h){
    if(infow != w || infoh != h){
        infow = w;
        infoh = h;
        info = info.scaled(QSize(w,h));
    }
    glDisable(GL_LIGHTING);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glDrawPixels(info.width(), info.height(), GL_RGBA, GL_UNSIGNED_BYTE, info.bits());
    glEnable(GL_LIGHTING);
}
