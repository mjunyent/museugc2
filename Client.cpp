#include "Client.h"

//TODO: utilitzar hton i ntoh al enviar i rebre nombres entre client i servidor
//      utilitzar accessos als vectors missatges i users amb semàfors!

Client::Client(QObject *parent, Personatge* personatge) :
    QObject(parent)
{
    //inicialitzem les variables
    connectat = false;
    loggedIn = false;
    rebentNovetats = false;
    numUsers = 0;
    pers = personatge;
    users.reserve(10);
}

Client::~Client()
{
    if (connectat){
        close(sock);
    }
}

bool Client::rebre(char* bufferRebre, int length){
    int recvMsgSize, res;
    fd_set fds;
    struct timeval timeout;

    FD_ZERO(&fds);
    FD_SET(sock, &fds);

    timeout.tv_sec =  SEC_MAX_REBRE;
    timeout.tv_usec = USEC_MAX_REBRE;

    if((res = select(sock+1, &fds, NULL, NULL, &timeout)) > 0){
        recvMsgSize = recv(sock, bufferRebre, length, 0);
        if(recvMsgSize == length){
            return true;
        } else {
            desconnecta();
        }
    } else if(res == -1){
        //Si rebem un -1 vol dir que el servidor ha tallat la connexió, desconnectem.
        //Si és 0 vol dir que no hem rebut res i per tant no ens desconnectem
        desconnecta();
    }
    return false;
}

void Client::desconnecta(){
    connectat = false;
    loggedIn = false;
    close(sock);
}

//TODO: crear l'estructura del client i fer bind amb el socket
bool Client::connecta(char *host, unsigned short serverPort)
{
    struct hostent *phe;
    struct timeval Timeout;
    fd_set Write, Err;
    long arg;

    Timeout.tv_sec = SEC_MAX_CONNECTA;
    Timeout.tv_usec = 0;

    // Create a reliable, stream socket using TCP
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) >= 0){
        // Construct the server address structure
        serverAddr.sin_family      = AF_INET;               // Internet address family
        serverAddr.sin_port        = htons(serverPort);     // Server port
        if ( (phe = gethostbyname(host)) ){
            bcopy(phe->h_addr, (char *)&serverAddr.sin_addr, phe->h_length);

            // Set non-blocking
            if( (arg = fcntl(sock, F_GETFL, NULL)) < 0) {
                return false;
            }
            arg |= O_NONBLOCK;
            if( fcntl(sock, F_SETFL, arg) < 0) {
                return false;
            }

            // Establish the connection to the server
            if (::connect(sock, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) == false){
                return false;
            }

            // Set to blocking mode again...
            if( (arg = fcntl(sock, F_GETFL, NULL)) < 0) {
                return false;
            }
            arg &= (~O_NONBLOCK);
            if( fcntl(sock, F_SETFL, arg) < 0) {
                return false;
            }

            FD_ZERO(&Write);
            FD_ZERO(&Err);
            FD_SET(sock, &Write);
            FD_SET(sock, &Err);

            // check if the socket is ready
            select(sock+1,NULL,&Write,&Err,&Timeout);
            if (Timeout.tv_sec == 0 && Timeout.tv_usec == 0)
                return false;
            if(FD_ISSET(sock, &Write))
            {
                connectat = true;
                return true;
            }
        }
    }
    return false;
}

bool Client::logIn(char *nom)
{
    char bufferEnviar[sizeof(char) + sizeof(user)]; //opcode + estructura user
    char bufferRebre[sizeof(char) + sizeof(int)]; //opcode + ID
    user dades;
    int length;

    strcpy(pers->nom, nom); //ens guardem el nom
    id = -1;
    pers->id = id;
    if (connectat){
        //creem l'estructura a enviar
        bufferEnviar[0] = LOGIN;
        dades.id = id; //el servidor ens retornara la nostra id
        dades.x = pers->x;
        dades.y = pers->y;
        dades.z = pers->z;
        dades.orientation = pers->orientacioh;
        strcpy(dades.username, nom);

        memcpy(bufferEnviar+1, &dades, sizeof(user));
        //enviem la peticio de LOG IN
        length = sizeof(bufferEnviar);
        if(send(sock, bufferEnviar, length, 0) == length){
            //esperem rebre l'acceptacio del logIN, en cas contrari retornem false
            if (rebre(bufferRebre, sizeof(char))){
                if (*bufferRebre == ACCEPT){
                    if (rebre(bufferRebre, sizeof(int))){
                        id = (int)*bufferRebre; //copiem la id a l'objecte personatge
                        pers->id = id;
                        loggedIn = true;
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

bool Client::logOut()
{
    char opcode = LOGOUT;
    char bufferRebre;
    int length;
    //enviem la peticio de LOG OUT

    if (connectat){
        length = sizeof(char);
        if(send(sock, &opcode, length, 0) == length){
            //esperem rebre l'accept del logIN, en cas contrari retornem false
            if (rebre(&bufferRebre, sizeof(char))){
                if (bufferRebre == ACCEPT){
                    loggedIn = false;
                    return true;
                }
            }
        }
    }
    return false;
}

bool Client::enviarMissatge(char *missatge, int toID)
{
    char bufferEnviar[sizeof(char) + sizeof(message)]; //opcode + missatge
    int length;
    message dades;

    if (connectat){
        //creem l'estructura de les dades
        dades.fromID = id;
        dades.toID = toID; //en cas que no ens especifiquin id de destinatari sera per a tothom
        strcpy(dades.content, missatge);

        //creem el buffer a enviar
        bufferEnviar[0] = MESSAGE;
        memcpy(bufferEnviar+1, &dades, sizeof(message));
        //enviem el missatge
        length = sizeof(bufferEnviar);
        if(send(sock, bufferEnviar, length, 0) == length)
            return true;
    }
    return false;
}

bool Client::enviarMissatgeGlobal(char *missatge)
{
    return enviarMissatge(missatge, -1); //-1 significa que és per a tothom
}

bool Client::enviarMissatgePrivat(char *missatge, int toID)
{
    return enviarMissatge(missatge, toID);
}

void *handle(void* client){
    ((Client*)client)->funcioRebreNovetats();
    return NULL;
}

//TODO: accedir a users de forma exclusiva
void Client::funcioRebreNovetats(){
    char opcode;
    int* idLogout;
    char bufferRebre[BUFFER_SIZE];

    user *usuari;
    message *missatge;
    int ultimUsuariRegistrat = -1;

    rebentNovetats = true;
    while (connectat && loggedIn){
        //Ens bloquejem esperant alguna cosa del servidor. Tenim un timeout perquè no es quedi bloquejat per sempre i consulti les variables connectat i loggedIN
        if(rebre(bufferRebre, sizeof(char))){
            opcode = *bufferRebre;
            switch(opcode){
                case MESSAGE:
                    //rebem el missatge destinat a tothom
                    if(rebre(bufferRebre, sizeof(message))){
                        missatge = (message*)bufferRebre;
                        missatges.push_back(*missatge);
                    }
                    break;
                case USER:
                    //rebem el nou usuari
                    if(rebre(bufferRebre, sizeof(user))){
                        usuari = (user*)bufferRebre;
                        //mirem si es tracta d'un nou user
                        if(usuari->id != id){
                            if (ultimUsuariRegistrat < usuari->id){
                                //es tracta d'un usuari nou!
                                Personatge nouPers;

                                ultimUsuariRegistrat = usuari->id;
                                memcpy(nouPers.nom, usuari->username, USERNAME_LENGTH-1);
                                nouPers.nom[USERNAME_LENGTH-1] = '\0'; //per si de cas ens vingues un string més llarg del servidor
                                nouPers.x = usuari->x;
                                nouPers.y = usuari->y;
                                nouPers.z = usuari->z;
                                nouPers.orientacioh = usuari->orientation;
                                nouPers.id = usuari->id;
                                nouPers.setPersona(&(persones->at(0)));
                                users.push_back(nouPers);
                            } else {
                                //es tracta d'un usuari que ja tenim, el busquem i el modifiquem
                                for(std::vector<Personatge>::iterator u = users.begin(); u != users.end(); ++u){
                                    if(u->id == usuari->id){
                                        u->x = usuari->x;
                                        u->y = usuari->y;
                                        u->z = usuari->z;
                                        u->orientacioh = usuari->orientation;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case LOGOUT:
                    //un usuari s'ha desconnectat
                    if(rebre(bufferRebre, sizeof(int))){
                        idLogout = (int*)(bufferRebre);
                        for(std::vector<Personatge>::iterator u = users.begin(); u != users.end(); ++u){
                            if(u->id == *idLogout){
                                users.erase(u);
                                break;
                            }
                        }
                    }
                    break;
                default:
                    //rebem alguna cosa desconneguda, tallem la connexió
                    desconnecta();
                    connectat = false;
                    break;
            }
        }
    }
    if (connectat)
        desconnecta();
    rebentNovetats = false;
    pthread_exit(0);
}

bool Client::rebreNovetats(std::vector<Persona>* persones){
    this->persones = persones;
    //posem un thread a rebreNovetats
    if (!rebentNovetats && connectat && loggedIn){
        if (pthread_create(&thread, NULL, handle, this) != 0)
            return false;
        else
            return true;
    }
    return false;
}

bool Client::enviaPosicio(){
    char bufferEnviar[sizeof(char) + sizeof(user)]; //opcode + missatge
    user posicio;
    int length;

    if (connectat){
        //preparem l'estructura
        posicio.id = id;
        strcpy(posicio.username, pers->nom);
        posicio.orientation = pers->orientacioh;
        posicio.x = pers->x;
        posicio.y = pers->y;
        posicio.z = pers->z;

        //preparem el buffer i l'enviem
        bufferEnviar[0] = USER;
        memcpy(bufferEnviar+1, &posicio, sizeof(user));
        length = sizeof(bufferEnviar);
        if(send(sock, bufferEnviar, length, 0) == length)
            return true;
    }
    return false;
}

//Aquesta funcio serveix per descartar qualsevol cosa rebuda del client
//normalment s'utilitzarà després de fer logout i abans de tornar a fer login
//Deixa el socket a punt per a rebre alguna cosa totalment nova
bool Client::descartaBrosaRebuda(){
    int recvMsgSize, res;
    char bufferRebre[512];
    fd_set fds;
    struct timeval timeout;

    //timeout a 0 perquè el select no s'esperi
    timeout.tv_sec = 0;
    timeout.tv_usec = 0;

    FD_ZERO(&fds); //Inicialitzem el conjunt a 0
    FD_SET(sock, &fds); // Posem el nostre socket al conjunt

    if (connectat){
        while (connectat && (res = select(sock+1, &fds, NULL, NULL, &timeout)) > 0){
            if ((recvMsgSize = recv(sock, bufferRebre, sizeof(bufferRebre), 0)) < 1){
                //tant si rebem un 0 (acabar la connexió bé) com si rebem un -1 (acabar la connexió de manera forçada -RESET-), desconnectem
                connectat = false;
                close(sock);
            }
        }
        if (res < 0){ //el select ha donat un error, acabem la connexió
            connectat = false;
            close(sock);
        }
    }
    return connectat;
}

void Client::processaMissatges(){
    for(std::vector<message>::iterator m = missatges.begin(); m != missatges.end(); ++m){
        //Busquem el nom de l'usuari que envia el missatge
        for(std::vector<Personatge>::iterator u = users.begin(); u != users.end(); ++u){
            if(u->id == m->fromID){
                if(m->toID == id) //missatge privat
                    emit nouMissatge(QString(u->nom), QString(m->content), true);
                else if (m->toID == -1) //missatge global
                    emit nouMissatge(QString(u->nom), QString(m->content), false);
                //Eliminem el missatge de la llista de missatges pendents
                break;
            }
        }
    }
    missatges.clear();
}

void Client::dibuixaUsuaris(){
    for(std::vector<Personatge>::iterator u = users.begin(); u != users.end(); ++u){
        u->dibuixa();
    }
}
