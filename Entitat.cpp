#include "Entitat.h"

Entitat::Entitat()
{
}

void Entitat::dibuixa(Shader* shader, Frustum* frustum)
{
    int i;

    for (i=0;i<this->objecte.size();i++)
    {
        if(frustum->sphereInFrustum(objecte[i].centre[0], objecte[i].centre[1], objecte[i].centre[2], objecte[i].radi)){
            if(objecte[i].hasNormalTexture){
                shader->activaBumpMapping();
                shader->bump->setUniformValue("normalMap", 1);
                this->objecte[i].dibuixaBump(shader->bump, shader->tangentLoc, shader->bitangentLoc);
            }else if (objecte[i].brillant){
                shader->activaPhong();
                this->objecte[i].dibuixa();
            }else{
                shader->activaGouraud();
                this->objecte[i].dibuixa();
            }
        }
    }
}

void Entitat::dibuixaSimple(Shader* shader, Frustum* frustum, bool exigimColisions){
    int i;
    for (i=0;i<this->objecte.size();i++)
    {
        if(frustum->sphereInFrustum(objecte[i].centre[0], objecte[i].centre[1], objecte[i].centre[2], objecte[i].radi)){
            shader->activaSimple();
            this->objecte[i].dibuixa(exigimColisions);
        }
    }
}
