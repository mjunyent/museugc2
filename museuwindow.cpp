#include "museuwindow.h"
#include "ui_museuwindow.h"
MuseuWindow::MuseuWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MuseuWindow)
{
    ui->setupUi(this);
    primercop = true;
    // ALERT! Use the following code to get keyboard focus at your OpenGL widget
    ui->contextGL->setFocusPolicy(Qt::StrongFocus);
    ui->contextGL->setFocus();

    this->setCursor(Qt::ArrowCursor);
    fullscreen = false;
    //fem que l'stackedWidget no es vegui (és on hi ha tots els menus)
    ui->stackedWidget->setVisible(false);
    ui->stackedWidget->setStyleSheet("background-color: grey;");
    ui->xatWidget->setStyleSheet("background-color: grey;");
    //Fem que el xat estigui inactiu ja que inicialment no estem connectats a cap servidor
    ui->xatWidget->setVisible(false);
    //Fem que no es pugui escriure al xat

    //Conectem la senyal mostraMenu() del contextGL amb l'slot mostraMenu nostre (no cal que tinguin el mateix nom!)
    connect(ui->contextGL, SIGNAL(mostraMenu()), this, SLOT(mostraMenu()), Qt::QueuedConnection); //necessitem QueuedConnection ja que utilitzem threads!
    connect(ui->contextGL, SIGNAL(continua()), this, SLOT(continua()), Qt::QueuedConnection);
    //Connectem el senyal escriureXat() del contextGL amb l'slot setFocus del xatLine de manera que al apretar enter, es genera el senyal i es fa focus al xatLine.
    connect(ui->contextGL, SIGNAL(escriureXat()), this, SLOT(escriureXat()), Qt::QueuedConnection);
    connect(ui->contextGL->client, SIGNAL(nouMissatge(QString,QString,bool)), this, SLOT(nouMissatge(QString,QString,bool)), Qt::QueuedConnection);
    connect(ui->xatLine, SIGNAL(editingFinished()), this, SLOT(xatEditingFinished()), Qt::QueuedConnection);
    connect(ui->shaderComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(canviaShaders(int)), Qt::QueuedConnection);
    connect(ui->screenModeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(canviaScreenMode(int)), Qt::QueuedConnection);
    connect(ui->personModeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(canviaModeJoc(int)), Qt::QueuedConnection);
    connect(ui->personesComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(canviaPersona(int)), Qt::QueuedConnection);
    connect(ui->contextGL, SIGNAL(amagaXat()), this, SLOT(amagaXat()), Qt::QueuedConnection);
    connect(ui->contextGL, SIGNAL(mostraXat()), this, SLOT(mostraXat()), Qt::QueuedConnection);
    connect(ui->contextGL, SIGNAL(canviaVisualitzacio()), this, SLOT(canviaVisualitzacio()), Qt::QueuedConnection);
}

MuseuWindow::~MuseuWindow()
{
    delete ui;
}

void MuseuWindow::mostraMenu(){
    ui->stackedWidget->setCurrentIndex(0); //Index 0 = menu
    ui->stackedWidget->setVisible(true);
    this->setCursor(Qt::ArrowCursor);
}

void MuseuWindow::continua(){
    ui->stackedWidget->setVisible(false);
    ui->contextGL->setFocus();
    ui->contextGL->activarControl();
    if(fullscreen) //només treiem el cursor si estem en mode fullscreen
        this->setCursor(Qt::BlankCursor);
}

void MuseuWindow::mostraAboutUs(){
    ui->stackedWidget->setCurrentIndex(1);
}

void MuseuWindow::mostraConnectToServer(){
    if(ui->contextGL->client->connectat){
        //Si ja estem connectats a un servidor, desactivem els dialegs
        ui->reportLabel->setText(QString("<font color='green'>Connected  :D</font>"));
        ui->serverLineEdit->setReadOnly(true);
        ui->portLineEdit->setReadOnly(true);
        ui->userLineEdit->setReadOnly(true);
    } else {
        ui->reportLabel->setText(QString(""));
        ui->serverLineEdit->setReadOnly(false);
        ui->portLineEdit->setReadOnly(false);
        ui->userLineEdit->setReadOnly(false);
    }
    ui->stackedWidget->setCurrentIndex(2);
}

void MuseuWindow::mostraOptions(){
    if(primercop){
        QStringList llistaItems;
        primercop = false;
        for(int i=0; i<ui->contextGL->Persones.size(); i++){
            llistaItems.push_back(QString(ui->contextGL->Persones[i].nom));
        }
        ui->personesComboBox->addItems(llistaItems);
    }
    if(ui->contextGL->sight3rd == 0){
        //cas que estiguem en primera persona
        ui->personModeComboBox->setCurrentIndex(0);
    } else {
        ui->personModeComboBox->setCurrentIndex(1);
    }
    ui->stackedWidget->setCurrentIndex(3);
}

void MuseuWindow::sortir(){
    exit(0);
}

void MuseuWindow::connectToServer(){
    if(!ui->contextGL->client->connectat){
        //si no estem connectats
        if(ui->contextGL->client->connecta(ui->serverLineEdit->text().toAscii().data(), ui->portLineEdit->text().toShort())){
            //connectat
            if(ui->contextGL->client->logIn(ui->userLineEdit->text().toAscii().data())){
                //Hem pogut fer login!
                if(ui->contextGL->client->rebreNovetats(&(ui->contextGL->Persones))){
                    //Estem rebent novetats
                    ui->serverLineEdit->setReadOnly(true);
                    ui->portLineEdit->setReadOnly(true);
                    ui->userLineEdit->setReadOnly(true);
                    ui->reportLabel->setText(QString("<font color='green'>Connected  :D</font>"));
                    ui->xatWidget->setVisible(true);
                }
            } else {
                ui->contextGL->client->desconnecta();
                ui->reportLabel->setText(QString("<font color='red'>Username already used.</font>"));
            }
        } else {
            //no s'ha pogut connectar
            ui->reportLabel->setText(QString("<font color='red'>Cannot connect to the server.</font>"));
        }
    }
}

void MuseuWindow::canviaScreenMode(int index)
{
    switch(index){
    case 0:
        fullscreen = false;
        this->showNormal();
        this->showMaximized();
        this->setCursor(Qt::ArrowCursor);
        break;
    case 1:
        fullscreen = true;
        this->showFullScreen(); //activem pantalla completa
        break;
    default:
        break;
    }
}

void MuseuWindow::canviaPersona(int index)
{
    if(index < ui->contextGL->Persones.size())
        ui->contextGL->pers->setPersona(&(ui->contextGL->Persones[index]));
}

void MuseuWindow::canviaShaders(int index)
{
    if(ui->contextGL->openGL2OK){
        switch(index){
        case 0: //Activats
            ui->contextGL->shadersON = true;
            break;
        case 1: //Desactivats
            ui->contextGL->shadersON = false;
            break;
        default:
            break;
        }
    }
}

void MuseuWindow::nouMissatge(QString nom, QString missatge, bool privat){
    if(privat)
        ui->xatText->append("<b>[Private] " + nom + "</b>: " + missatge);
    else
        ui->xatText->append("<b>" + nom + "</b>: " + missatge);
}

void MuseuWindow::escriureXat(){
    if(ui->xatWidget->isVisible())
        ui->xatLine->setFocus();
}

void MuseuWindow::xatEditingFinished()
{
    // What did they want to say (minus white space around the string):
    QString message = ui->xatLine->text().trimmed();

    // Only send the text to the chat server if it's not empty:
    if(!message.isEmpty())
    {
        //enviem el missatge
        ui->contextGL->client->enviarMissatgeGlobal(message.toAscii().data());
        //mostrem el missatge
        this->nouMissatge(QString("me"), QString(message), false);
    }
    // Clear out the input box so they can type something else:
    ui->xatLine->clear();
    ui->contextGL->setFocus();
}

void MuseuWindow::amagaXat(){
    ui->xatWidget->setVisible(false);
}

void MuseuWindow::mostraXat(){
    ui->xatWidget->setVisible(true);
}

void MuseuWindow::canviaModeJoc(int index){
    if(index == 0){
        //primera persona
        ui->contextGL->sight3rd = 0.0f;
    }else{
        //index = 1 -> tercera persona
        if(ui->contextGL->sight3rd == 0)//si estem en primera persona, canviem, sino ho deixem tal qual
            ui->contextGL->sight3rd = MAXIM_LLUNY;
    }
}

void MuseuWindow::canviaVisualitzacio(){
    if(ui->contextGL->sight3rd>0){
        //canviem a primera persona
        ui->personModeComboBox->setCurrentIndex(0);

    } else {
        //canviem a tercera persona
        ui->personModeComboBox->setCurrentIndex(1);
    }
}
